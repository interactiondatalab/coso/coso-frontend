import React, { useState, useEffect, useContext } from 'react';
import { Icon, Grid, Header, Segment, Button } from 'semantic-ui-react';
import "./Header.css";
import Avatar from 'react-avatar';
import IgemUserIDSelect from "../utils/IgemUserIDSelect";
import Api from "../Api"
import { UserContext } from "../utils/UserContext"

const UserItem = ({user, setRefresh}) => {
  const [username, setUsername] = useState("")
  const [editUsername, setEditUsername] = useState(false)
  const userContext = useContext(UserContext);
  const [hideUsernameInput, setHideUsernameInput] = useState(true);

  useEffect(() => {
    if (user.custom) {
      setHideUsernameInput(false);
    }
    setUsername(user.username)
  }, [user])

  const saveUserId = (value) => {
    console.log(value)
    console.log(user)
    const igem_user = {
      user_id: user.user.id,
    }
    Api.patch(`igem_users/${value}`, {igem_user})
    .then(res => {
      Api.delete(`igem_users/${user.id}`)
      .then(res => {
        setHideUsernameInput(true);
        setRefresh(true);
      })
      user = res.data
    })
    .catch(error => {
      console.log(error)
    })
  }

  console.log(user)

  return (
  <Segment attached padded>
    <Grid columns='equal'  verticalAlign="middle">
      <Grid.Column verticalAlign="middle">
        <Header verticalAlign="middle">
          <Avatar className="ui mini image avatar" name={user.full_name} alt={user.full_name} size="32px" round="2" email={user.user == null ? "" : user.user.email} />
          {user.full_name}
        </Header>
      </Grid.Column>
      <Grid.Column>
        {
          hideUsernameInput ?
            <span>UserName: {username}</span>
          :
            <>
            <span className="ui red basic label" style={{border:"0"}}>Missing iGEM username</span>
            {editUsername ?
              <IgemUserIDSelect
                teamId={userContext.user.team.id}
                setIgemUserId={saveUserId}
                save={true}
              />
              :
              <Button compact basic size="mini" onClick={() => {setEditUsername(true)}}>+Add iGEM username</Button>
            }
            </>
        }
      </Grid.Column>
      <Grid.Column>
        {user.user == null ? "0" : user.user.log_count} Tasks
      </Grid.Column>
      <Grid.Column>
        <Grid columns={3}>
          <Grid.Row  style={{paddingTop:"0.2rem", paddingBottom:"0.4rem"}}>
            <Grid.Column width={1}>
              <Icon name="computer" disabled={user.user == null} />
            </Grid.Column>
            <Grid.Column width={4}>
              <span disabled={user.user == null}>Website</span>
            </Grid.Column>
            <Grid.Column width={1}>
              {user.user == null ? <Icon color="red" name="times circle outline" /> : <Icon color="green" name="check circle outline" />}
            </Grid.Column>
          </Grid.Row>
          <Grid.Row style={{paddingTop:"0.2rem", paddingBottom:"0.4rem"}}>
            <Grid.Column width={1}>
              <Icon name="mobile alternate" disabled={user.user == null ? true : !user.user.phones.length > 0}/>
            </Grid.Column>
            <Grid.Column width={4}>
              <span disabled={user.user == null ? true : !user.user.phones.length > 0}>App</span>
            </Grid.Column>
            <Grid.Column width={1}>
              {user.user == null ? <Icon color="red" name="times circle outline" /> : !user.user.phones.length > 0 ? <Icon color="red" name="times circle outline" /> : <Icon color="green" name="check circle outline" />}
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Grid.Column>
    </Grid>
  </Segment>
)};

const Users = ({team, setRefresh}) => {

  const users = team.map(function(user, i){
    return <UserItem user={user} key={i} setRefresh={setRefresh} />;
  })

  return(
    <Segment basic>
    <Header block attached="top">
      <Grid columns='equal'>
          <Grid.Column>
            Team members
          </Grid.Column>
          <Grid.Column>
            iGEM Username
          </Grid.Column>
          <Grid.Column>
            Activity
          </Grid.Column>
          <Grid.Column>
            Registered on
          </Grid.Column>
      </Grid>
    </Header>
    {users}
    </Segment>
  )
}

export default Users

export { UserItem, Users };
