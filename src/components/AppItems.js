import React from 'react';
import { Icon, Grid, Header, Image, Segment, Button } from 'semantic-ui-react';
import "./Header.css";

import SlackAppItem from "./apps/SlackAppItem";

const AppItem = ({name, icon, description, cta, status }) => (
  <Segment attached padded>
    <Grid columns='equal' verticalAlign="middle">
      <Grid.Column width={4}>
        <Header verticalAlign="middle">
          <Image alt={name} size="32px" src={icon}/>
          {name}
        </Header>
      </Grid.Column>
      <Grid.Column width={8}>
        {description}
      </Grid.Column>
      <Grid.Column width={3}  textAlign="right">
        {cta}
      </Grid.Column>
      <Grid.Column width={1} textAlign="right">
        {status ?
        <Icon name="linkify" color="teal" />
        :
        <Icon name="unlinkify" />}
      </Grid.Column>
    </Grid>
  </Segment>
)

const Apps = () => {

  return(
    <Segment basic>
    <Header block attached="top">
      <Grid columns='equal'>
          <Grid.Column width={4}>
            Apps name
          </Grid.Column>
          <Grid.Column width={8}>

          </Grid.Column>
          <Grid.Column width={3}>

          </Grid.Column>
          <Grid.Column width={1}>
            Status
          </Grid.Column>
      </Grid>
    </Header>
    <SlackAppItem />
    <AppItem
      name="Wiki"
      icon="https://igem.org/wiki/images/c/cf/Igem_icon.svg"
      description="Your insights from your iGEM wiki account with CoSo!"
      cta={<Button>Disconnect</Button>}
      status={true}
    />
    </Segment>
  )
}

export default Apps

export { AppItem, Apps };
