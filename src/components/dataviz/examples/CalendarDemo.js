import CoSoCalendar from "../Calendar"
import {DatavizCardFull} from "../../DatavizCards";
import { useState } from "react";

function CalendarDemo(props) {
  const [loading, setLoading] = useState(true);

  setTimeout(() => {
    setLoading(false)
  }, 3000)

  const data = [
  {
    "day": "2018-04-17",
    "value": 256
  },
  {
    "day": "2016-03-03",
    "value": 298
  },
  {
    "day": "2017-08-30",
    "value": 18
  },
  {
    "day": "2018-05-18",
    "value": 384
  },
  {
    "day": "2016-07-13",
    "value": 243
  },
  {
    "day": "2016-04-24",
    "value": 370
  },
  {
    "day": "2017-01-29",
    "value": 5
  },
  {
    "day": "2016-10-11",
    "value": 65
  },
  {
    "day": "2018-01-22",
    "value": 337
  },
  {
    "day": "2017-09-29",
    "value": 342
  },
  {
    "day": "2017-05-20",
    "value": 298
  },
  {
    "day": "2015-08-21",
    "value": 396
  },
  {
    "day": "2016-03-24",
    "value": 3
  },
  {
    "day": "2017-08-08",
    "value": 181
  },
  {
    "day": "2017-02-23",
    "value": 248
  },
  {
    "day": "2015-06-16",
    "value": 255
  },
  {
    "day": "2017-08-06",
    "value": 341
  },
  {
    "day": "2018-04-23",
    "value": 240
  },
  {
    "day": "2018-01-06",
    "value": 324
  },
  {
    "day": "2016-07-21",
    "value": 211
  },
  {
    "day": "2016-02-03",
    "value": 160
  },
  {
    "day": "2016-07-20",
    "value": 250
  },
  {
    "day": "2018-02-15",
    "value": 70
  },
  {
    "day": "2017-12-27",
    "value": 213
  },
  {
    "day": "2016-09-28",
    "value": 87
  },
  {
    "day": "2016-04-23",
    "value": 270
  },
  {
    "day": "2016-05-30",
    "value": 164
  },
  {
    "day": "2017-08-13",
    "value": 124
  },
  {
    "day": "2018-04-14",
    "value": 188
  },
  {
    "day": "2018-06-21",
    "value": 79
  },
  {
    "day": "2016-02-09",
    "value": 245
  },
  {
    "day": "2016-11-18",
    "value": 348
  },
  {
    "day": "2017-11-28",
    "value": 281
  },
  {
    "day": "2016-05-28",
    "value": 247
  },
  {
    "day": "2016-06-18",
    "value": 130
  },
  {
    "day": "2016-02-06",
    "value": 209
  },
  {
    "day": "2016-03-18",
    "value": 201
  },
  {
    "day": "2017-02-18",
    "value": 229
  },
  {
    "day": "2016-07-18",
    "value": 150
  },
  {
    "day": "2016-04-20",
    "value": 80
  },
  {
    "day": "2016-09-12",
    "value": 252
  },
  {
    "day": "2017-08-09",
    "value": 396
  },
  {
    "day": "2015-10-08",
    "value": 371
  },
  {
    "day": "2017-01-13",
    "value": 215
  },
  {
    "day": "2015-10-31",
    "value": 35
  },
  {
    "day": "2017-05-19",
    "value": 195
  },
  {
    "day": "2015-12-04",
    "value": 365
  },
  {
    "day": "2016-06-10",
    "value": 123
  },
  {
    "day": "2017-02-16",
    "value": 166
  },
  {
    "day": "2017-07-16",
    "value": 135
  },
  {
    "day": "2017-07-21",
    "value": 85
  },
  {
    "day": "2017-03-10",
    "value": 295
  },
  {
    "day": "2016-10-26",
    "value": 11
  },
  {
    "day": "2017-01-02",
    "value": 192
  },
  {
    "day": "2016-12-27",
    "value": 388
  },
  {
    "day": "2016-11-22",
    "value": 222
  },
  {
    "day": "2016-11-29",
    "value": 148
  },
  {
    "day": "2016-10-20",
    "value": 200
  },
  {
    "day": "2017-02-12",
    "value": 321
  },
  {
    "day": "2016-03-09",
    "value": 303
  },
  {
    "day": "2015-08-07",
    "value": 112
  },
  {
    "day": "2016-10-29",
    "value": 373
  },
  {
    "day": "2015-07-06",
    "value": 353
  },
  {
    "day": "2016-02-22",
    "value": 70
  },
  {
    "day": "2018-07-31",
    "value": 382
  },
  {
    "day": "2016-03-19",
    "value": 260
  },
  {
    "day": "2018-03-27",
    "value": 308
  },
  {
    "day": "2016-09-16",
    "value": 173
  },
  {
    "day": "2015-04-27",
    "value": 133
  },
  {
    "day": "2018-03-05",
    "value": 369
  },
  {
    "day": "2016-01-11",
    "value": 71
  },
  {
    "day": "2017-01-30",
    "value": 210
  },
  {
    "day": "2016-11-09",
    "value": 391
  },
  {
    "day": "2017-09-06",
    "value": 315
  },
  {
    "day": "2018-06-26",
    "value": 207
  },
  {
    "day": "2015-12-13",
    "value": 195
  },
  {
    "day": "2015-08-25",
    "value": 234
  },
  {
    "day": "2015-07-22",
    "value": 309
  },
  {
    "day": "2018-07-07",
    "value": 197
  },
  {
    "day": "2018-01-26",
    "value": 291
  },
  {
    "day": "2017-12-09",
    "value": 193
  },
  {
    "day": "2016-05-08",
    "value": 71
  },
  {
    "day": "2016-06-06",
    "value": 226
  },
  {
    "day": "2015-10-25",
    "value": 180
  },
  {
    "day": "2015-05-07",
    "value": 229
  },
  {
    "day": "2015-08-04",
    "value": 249
  },
  {
    "day": "2016-01-30",
    "value": 199
  },
  {
    "day": "2016-02-17",
    "value": 334
  },
  {
    "day": "2018-01-05",
    "value": 390
  },
  {
    "day": "2015-05-30",
    "value": 375
  },
  {
    "day": "2018-01-13",
    "value": 349
  },
  {
    "day": "2017-04-04",
    "value": 203
  },
  {
    "day": "2016-12-25",
    "value": 263
  },
  {
    "day": "2016-05-22",
    "value": 136
  },
  {
    "day": "2016-09-14",
    "value": 164
  },
  {
    "day": "2016-08-18",
    "value": 89
  },
  {
    "day": "2018-04-01",
    "value": 245
  },
  {
    "day": "2017-10-04",
    "value": 175
  },
  {
    "day": "2015-04-07",
    "value": 5
  },
  {
    "day": "2017-11-26",
    "value": 170
  },
  {
    "day": "2018-05-25",
    "value": 116
  },
  {
    "day": "2015-09-22",
    "value": 128
  },
  {
    "day": "2017-05-07",
    "value": 169
  },
  {
    "day": "2015-05-19",
    "value": 127
  },
  {
    "day": "2017-10-24",
    "value": 12
  },
  {
    "day": "2017-02-05",
    "value": 25
  },
  {
    "day": "2015-05-23",
    "value": 82
  },
  {
    "day": "2017-07-02",
    "value": 349
  },
  {
    "day": "2016-12-14",
    "value": 339
  },
  {
    "day": "2016-05-26",
    "value": 292
  },
  {
    "day": "2016-02-27",
    "value": 96
  },
  {
    "day": "2015-12-01",
    "value": 352
  },
  {
    "day": "2015-07-11",
    "value": 69
  },
  {
    "day": "2016-05-06",
    "value": 81
  },
  {
    "day": "2016-09-30",
    "value": 125
  },
  {
    "day": "2016-01-25",
    "value": 193
  },
  {
    "day": "2016-01-08",
    "value": 144
  },
  {
    "day": "2017-12-06",
    "value": 164
  },
  {
    "day": "2017-11-14",
    "value": 318
  },
  {
    "day": "2015-09-18",
    "value": 181
  },
  {
    "day": "2016-07-05",
    "value": 287
  },
  {
    "day": "2015-07-16",
    "value": 4
  },
  {
    "day": "2016-11-13",
    "value": 64
  },
  {
    "day": "2018-04-29",
    "value": 252
  },
  {
    "day": "2017-09-28",
    "value": 158
  },
  {
    "day": "2016-07-17",
    "value": 117
  },
  {
    "day": "2018-08-05",
    "value": 180
  },
  {
    "day": "2015-06-14",
    "value": 319
  },
  {
    "day": "2016-09-26",
    "value": 296
  },
  {
    "day": "2017-10-20",
    "value": 367
  },
  {
    "day": "2017-06-02",
    "value": 48
  },
  {
    "day": "2018-02-22",
    "value": 336
  },
  {
    "day": "2017-04-07",
    "value": 21
  },
  {
    "day": "2017-08-31",
    "value": 287
  },
  {
    "day": "2017-10-01",
    "value": 369
  },
  {
    "day": "2015-08-27",
    "value": 114
  },
  {
    "day": "2016-05-21",
    "value": 154
  },
  {
    "day": "2018-04-13",
    "value": 316
  },
  {
    "day": "2016-11-21",
    "value": 280
  },
  {
    "day": "2016-09-20",
    "value": 252
  },
  {
    "day": "2017-01-27",
    "value": 265
  },
  {
    "day": "2016-08-24",
    "value": 236
  },
  {
    "day": "2017-03-09",
    "value": 286
  },
  {
    "day": "2015-05-20",
    "value": 397
  },
  {
    "day": "2017-02-09",
    "value": 155
  },
  {
    "day": "2017-05-22",
    "value": 379
  },
  {
    "day": "2017-06-14",
    "value": 29
  },
  {
    "day": "2015-05-27",
    "value": 296
  },
  {
    "day": "2016-07-07",
    "value": 173
  },
  {
    "day": "2016-05-11",
    "value": 319
  },
  {
    "day": "2017-11-17",
    "value": 51
  },
  {
    "day": "2018-04-12",
    "value": 394
  },
  {
    "day": "2015-10-11",
    "value": 300
  },
  {
    "day": "2015-06-17",
    "value": 214
  },
  {
    "day": "2018-07-17",
    "value": 39
  },
  {
    "day": "2017-12-01",
    "value": 71
  },
  {
    "day": "2017-07-08",
    "value": 33
  },
  {
    "day": "2015-11-16",
    "value": 171
  },
  {
    "day": "2016-08-13",
    "value": 50
  },
  {
    "day": "2018-05-19",
    "value": 135
  },
  {
    "day": "2016-08-20",
    "value": 325
  },
  {
    "day": "2016-02-21",
    "value": 372
  },
  {
    "day": "2017-10-08",
    "value": 389
  },
  {
    "day": "2016-12-08",
    "value": 235
  },
  {
    "day": "2018-05-06",
    "value": 6
  },
  {
    "day": "2015-04-17",
    "value": 174
  },
  {
    "day": "2016-06-26",
    "value": 396
  },
  {
    "day": "2018-03-01",
    "value": 271
  },
  {
    "day": "2017-05-30",
    "value": 264
  },
  {
    "day": "2016-03-23",
    "value": 301
  },
  {
    "day": "2016-05-25",
    "value": 283
  },
  {
    "day": "2016-09-21",
    "value": 357
  },
  {
    "day": "2015-08-31",
    "value": 149
  },
  {
    "day": "2017-01-09",
    "value": 196
  },
  {
    "day": "2016-07-16",
    "value": 121
  },
  {
    "day": "2017-06-09",
    "value": 3
  },
  {
    "day": "2017-05-01",
    "value": 66
  },
  {
    "day": "2018-02-12",
    "value": 205
  },
  {
    "day": "2017-10-30",
    "value": 178
  },
  {
    "day": "2016-11-05",
    "value": 371
  },
  {
    "day": "2016-03-08",
    "value": 22
  },
  {
    "day": "2017-07-13",
    "value": 136
  },
  {
    "day": "2017-12-31",
    "value": 244
  },
  {
    "day": "2016-12-28",
    "value": 342
  },
  {
    "day": "2016-07-09",
    "value": 71
  },
  {
    "day": "2016-03-21",
    "value": 381
  },
  {
    "day": "2017-02-14",
    "value": 273
  },
  {
    "day": "2017-05-27",
    "value": 14
  },
  {
    "day": "2018-01-15",
    "value": 165
  },
  {
    "day": "2017-08-11",
    "value": 339
  },
  {
    "day": "2018-04-21",
    "value": 373
  },
  {
    "day": "2017-01-04",
    "value": 361
  },
  {
    "day": "2015-10-13",
    "value": 360
  },
  {
    "day": "2018-05-23",
    "value": 147
  },
  {
    "day": "2017-10-03",
    "value": 316
  },
  {
    "day": "2018-01-10",
    "value": 382
  },
  {
    "day": "2015-07-15",
    "value": 319
  },
  {
    "day": "2016-11-19",
    "value": 23
  },
  {
    "day": "2016-05-07",
    "value": 165
  },
  {
    "day": "2017-03-11",
    "value": 55
  },
  {
    "day": "2016-04-03",
    "value": 193
  },
  {
    "day": "2018-02-21",
    "value": 165
  },
  {
    "day": "2017-01-08",
    "value": 377
  },
  {
    "day": "2018-02-19",
    "value": 213
  },
  {
    "day": "2016-04-17",
    "value": 138
  },
  {
    "day": "2016-02-05",
    "value": 149
  },
  {
    "day": "2015-09-07",
    "value": 8
  },
  {
    "day": "2016-07-14",
    "value": 195
  },
  {
    "day": "2016-10-01",
    "value": 163
  },
  {
    "day": "2018-03-10",
    "value": 217
  },
  {
    "day": "2015-04-04",
    "value": 15
  },
  {
    "day": "2015-07-13",
    "value": 117
  },
  {
    "day": "2015-09-02",
    "value": 41
  },
  {
    "day": "2017-12-25",
    "value": 303
  },
  {
    "day": "2017-06-30",
    "value": 130
  },
  {
    "day": "2018-07-20",
    "value": 382
  },
  {
    "day": "2015-04-25",
    "value": 73
  },
  {
    "day": "2018-07-26",
    "value": 320
  },
  {
    "day": "2017-05-24",
    "value": 309
  },
  {
    "day": "2016-04-16",
    "value": 10
  },
  {
    "day": "2015-12-26",
    "value": 36
  },
  {
    "day": "2015-10-12",
    "value": 316
  },
  {
    "day": "2015-07-31",
    "value": 158
  },
  {
    "day": "2018-08-03",
    "value": 89
  },
  {
    "day": "2015-10-09",
    "value": 160
  },
  {
    "day": "2015-09-06",
    "value": 182
  },
  {
    "day": "2016-12-13",
    "value": 91
  },
  {
    "day": "2015-05-06",
    "value": 341
  },
  {
    "day": "2017-04-21",
    "value": 331
  },
  {
    "day": "2016-12-16",
    "value": 91
  },
  {
    "day": "2016-01-22",
    "value": 335
  },
  {
    "day": "2015-09-14",
    "value": 250
  },
  {
    "day": "2016-08-25",
    "value": 371
  },
  {
    "day": "2018-05-24",
    "value": 11
  },
  {
    "day": "2017-12-16",
    "value": 264
  },
  {
    "day": "2015-04-11",
    "value": 292
  },
  {
    "day": "2017-04-24",
    "value": 313
  },
  {
    "day": "2016-03-04",
    "value": 182
  },
  {
    "day": "2016-12-07",
    "value": 133
  },
  {
    "day": "2018-05-11",
    "value": 322
  },
  {
    "day": "2017-09-13",
    "value": 350
  },
  {
    "day": "2017-09-20",
    "value": 374
  },
  {
    "day": "2016-12-30",
    "value": 312
  },
  {
    "day": "2017-04-19",
    "value": 62
  },
  {
    "day": "2015-07-07",
    "value": 244
  },
  {
    "day": "2016-10-15",
    "value": 164
  },
  {
    "day": "2015-07-21",
    "value": 113
  },
  {
    "day": "2016-01-07",
    "value": 371
  },
  {
    "day": "2016-01-23",
    "value": 131
  },
  {
    "day": "2017-07-12",
    "value": 29
  },
  {
    "day": "2016-12-10",
    "value": 230
  },
  {
    "day": "2017-06-21",
    "value": 349
  },
  {
    "day": "2016-07-11",
    "value": 358
  },
  {
    "day": "2016-04-14",
    "value": 93
  },
  {
    "day": "2016-09-13",
    "value": 21
  },
  {
    "day": "2017-11-21",
    "value": 65
  },
  {
    "day": "2017-07-04",
    "value": 263
  },
  {
    "day": "2018-02-27",
    "value": 64
  },
  {
    "day": "2016-05-20",
    "value": 208
  },
  {
    "day": "2016-05-01",
    "value": 154
  },
  {
    "day": "2016-03-17",
    "value": 396
  },
  {
    "day": "2016-12-04",
    "value": 65
  },
  {
    "day": "2016-01-10",
    "value": 159
  },
  {
    "day": "2017-09-22",
    "value": 242
  },
  {
    "day": "2015-06-08",
    "value": 40
  },
  {
    "day": "2017-08-03",
    "value": 157
  },
  {
    "day": "2018-04-26",
    "value": 126
  },
  {
    "day": "2015-05-12",
    "value": 199
  },
  {
    "day": "2018-02-16",
    "value": 240
  },
  {
    "day": "2016-06-07",
    "value": 140
  },
  {
    "day": "2016-05-10",
    "value": 296
  },
  {
    "day": "2015-12-03",
    "value": 77
  },
  {
    "day": "2017-02-04",
    "value": 300
  },
  {
    "day": "2015-11-19",
    "value": 121
  },
  {
    "day": "2017-05-18",
    "value": 326
  },
  {
    "day": "2017-05-15",
    "value": 185
  },
  {
    "day": "2018-04-02",
    "value": 183
  },
  {
    "day": "2017-09-17",
    "value": 374
  },
  {
    "day": "2016-03-12",
    "value": 32
  },
  {
    "day": "2015-07-17",
    "value": 243
  },
  {
    "day": "2016-07-26",
    "value": 290
  },
  {
    "day": "2017-06-16",
    "value": 148
  },
  {
    "day": "2017-01-31",
    "value": 113
  },
  {
    "day": "2018-02-28",
    "value": 215
  },
  {
    "day": "2018-03-14",
    "value": 296
  },
  {
    "day": "2017-08-01",
    "value": 317
  },
  {
    "day": "2018-01-23",
    "value": 387
  },
  {
    "day": "2018-01-08",
    "value": 219
  },
  {
    "day": "2018-07-25",
    "value": 307
  },
  {
    "day": "2018-03-08",
    "value": 131
  },
  {
    "day": "2015-08-03",
    "value": 308
  },
  {
    "day": "2017-04-18",
    "value": 364
  },
  {
    "day": "2018-02-23",
    "value": 153
  },
  {
    "day": "2018-01-25",
    "value": 367
  },
  {
    "day": "2017-06-15",
    "value": 311
  },
  {
    "day": "2016-01-16",
    "value": 285
  },
  {
    "day": "2017-07-10",
    "value": 15
  },
  {
    "day": "2017-12-13",
    "value": 78
  },
  {
    "day": "2018-05-17",
    "value": 107
  },
  {
    "day": "2018-05-15",
    "value": 209
  },
  {
    "day": "2017-01-20",
    "value": 321
  },
  {
    "day": "2015-06-24",
    "value": 375
  },
  {
    "day": "2017-06-06",
    "value": 321
  },
  {
    "day": "2017-02-26",
    "value": 324
  },
  {
    "day": "2015-10-23",
    "value": 359
  },
  {
    "day": "2017-03-31",
    "value": 146
  },
  {
    "day": "2018-03-22",
    "value": 86
  },
  {
    "day": "2017-12-21",
    "value": 114
  },
  {
    "day": "2015-07-01",
    "value": 274
  },
  {
    "day": "2016-10-03",
    "value": 346
  },
  {
    "day": "2018-04-09",
    "value": 98
  },
  {
    "day": "2017-07-03",
    "value": 234
  },
  {
    "day": "2016-10-04",
    "value": 137
  },
  {
    "day": "2015-10-14",
    "value": 243
  },
  {
    "day": "2017-11-18",
    "value": 335
  },
  {
    "day": "2016-08-19",
    "value": 282
  },
  {
    "day": "2015-04-12",
    "value": 104
  },
  {
    "day": "2016-11-14",
    "value": 249
  },
  {
    "day": "2016-05-16",
    "value": 193
  },
  {
    "day": "2018-04-27",
    "value": 203
  },
  {
    "day": "2017-11-15",
    "value": 364
  },
  {
    "day": "2016-11-28",
    "value": 82
  },
  {
    "day": "2018-01-28",
    "value": 69
  },
  {
    "day": "2015-12-31",
    "value": 22
  },
  {
    "day": "2017-10-13",
    "value": 337
  },
  {
    "day": "2016-04-22",
    "value": 331
  },
  {
    "day": "2016-11-06",
    "value": 107
  },
  {
    "day": "2016-09-06",
    "value": 374
  },
  {
    "day": "2017-06-07",
    "value": 123
  },
  {
    "day": "2015-07-08",
    "value": 129
  },
  {
    "day": "2018-08-02",
    "value": 365
  },
  {
    "day": "2016-08-31",
    "value": 32
  },
  {
    "day": "2017-03-30",
    "value": 211
  },
  {
    "day": "2016-05-18",
    "value": 72
  },
  {
    "day": "2018-03-25",
    "value": 99
  },
  {
    "day": "2016-04-07",
    "value": 293
  },
  {
    "day": "2016-02-26",
    "value": 389
  },
  {
    "day": "2016-10-09",
    "value": 160
  },
  {
    "day": "2018-02-06",
    "value": 30
  },
  {
    "day": "2016-07-25",
    "value": 101
  },
  {
    "day": "2018-05-20",
    "value": 358
  },
  {
    "day": "2017-07-06",
    "value": 16
  },
  {
    "day": "2018-05-16",
    "value": 321
  },
  {
    "day": "2017-10-14",
    "value": 236
  },
  {
    "day": "2016-08-29",
    "value": 212
  },
  {
    "day": "2015-12-20",
    "value": 370
  },
  {
    "day": "2018-07-02",
    "value": 55
  },
  {
    "day": "2016-04-13",
    "value": 103
  },
  {
    "day": "2015-12-09",
    "value": 196
  },
  {
    "day": "2018-01-14",
    "value": 83
  },
  {
    "day": "2015-12-21",
    "value": 313
  },
  {
    "day": "2018-07-10",
    "value": 174
  },
  {
    "day": "2016-11-07",
    "value": 286
  },
  {
    "day": "2017-02-28",
    "value": 291
  },
  {
    "day": "2016-08-21",
    "value": 132
  },
  {
    "day": "2017-11-19",
    "value": 134
  },
  {
    "day": "2016-11-12",
    "value": 43
  },
  {
    "day": "2015-08-01",
    "value": 219
  },
  {
    "day": "2015-04-06",
    "value": 239
  },
  {
    "day": "2015-05-29",
    "value": 357
  },
  {
    "day": "2015-08-14",
    "value": 350
  },
  {
    "day": "2017-08-26",
    "value": 108
  },
  {
    "day": "2016-07-03",
    "value": 290
  },
  {
    "day": "2015-10-21",
    "value": 88
  },
  {
    "day": "2017-10-19",
    "value": 128
  },
  {
    "day": "2017-06-03",
    "value": 265
  },
  {
    "day": "2016-09-05",
    "value": 119
  },
  {
    "day": "2015-11-10",
    "value": 365
  },
  {
    "day": "2017-12-24",
    "value": 6
  },
  {
    "day": "2016-07-01",
    "value": 157
  },
  {
    "day": "2016-06-25",
    "value": 305
  },
  {
    "day": "2017-09-23",
    "value": 58
  },
  {
    "day": "2018-01-27",
    "value": 163
  },
  {
    "day": "2018-05-05",
    "value": 193
  },
  {
    "day": "2016-09-18",
    "value": 307
  },
  {
    "day": "2015-08-26",
    "value": 187
  },
  {
    "day": "2015-08-11",
    "value": 27
  },
  {
    "day": "2017-04-08",
    "value": 104
  },
  {
    "day": "2017-12-17",
    "value": 301
  },
  {
    "day": "2018-08-07",
    "value": 32
  },
  {
    "day": "2018-07-09",
    "value": 32
  },
  {
    "day": "2018-05-10",
    "value": 358
  },
  {
    "day": "2016-06-08",
    "value": 327
  },
  {
    "day": "2018-07-27",
    "value": 272
  },
  {
    "day": "2016-10-06",
    "value": 131
  },
  {
    "day": "2018-03-31",
    "value": 56
  },
  {
    "day": "2016-05-19",
    "value": 1
  },
  {
    "day": "2015-09-24",
    "value": 296
  },
  {
    "day": "2018-06-24",
    "value": 392
  },
  {
    "day": "2015-09-12",
    "value": 334
  },
  {
    "day": "2018-07-16",
    "value": 83
  },
  {
    "day": "2016-05-12",
    "value": 134
  },
  {
    "day": "2018-03-23",
    "value": 256
  },
  {
    "day": "2015-06-02",
    "value": 198
  },
  {
    "day": "2017-09-14",
    "value": 224
  },
  {
    "day": "2015-06-04",
    "value": 307
  },
  {
    "day": "2015-12-06",
    "value": 375
  },
  {
    "day": "2017-03-12",
    "value": 165
  },
  {
    "day": "2016-12-17",
    "value": 143
  },
  {
    "day": "2016-12-06",
    "value": 260
  },
  {
    "day": "2015-11-14",
    "value": 236
  },
  {
    "day": "2015-05-15",
    "value": 197
  },
  {
    "day": "2018-05-28",
    "value": 22
  },
  {
    "day": "2015-11-04",
    "value": 93
  },
  {
    "day": "2016-08-06",
    "value": 357
  },
  {
    "day": "2017-04-30",
    "value": 330
  },
  {
    "day": "2015-11-01",
    "value": 391
  },
  {
    "day": "2015-07-25",
    "value": 368
  },
  {
    "day": "2015-12-11",
    "value": 235
  },
  {
    "day": "2017-08-27",
    "value": 319
  },
  {
    "day": "2017-04-22",
    "value": 327
  },
  {
    "day": "2017-09-04",
    "value": 384
  },
  {
    "day": "2016-08-26",
    "value": 316
  },
  {
    "day": "2015-11-15",
    "value": 139
  },
  {
    "day": "2016-02-19",
    "value": 111
  },
  {
    "day": "2015-07-10",
    "value": 183
  },
  {
    "day": "2015-09-10",
    "value": 358
  },
  {
    "day": "2015-06-10",
    "value": 243
  },
  {
    "day": "2015-11-21",
    "value": 8
  },
  {
    "day": "2017-06-20",
    "value": 113
  },
  {
    "day": "2018-05-29",
    "value": 368
  },
  {
    "day": "2018-03-29",
    "value": 323
  },
  {
    "day": "2018-06-12",
    "value": 384
  },
  {
    "day": "2016-11-26",
    "value": 137
  },
  {
    "day": "2015-09-16",
    "value": 85
  },
  {
    "day": "2018-06-20",
    "value": 198
  },
  {
    "day": "2017-11-30",
    "value": 163
  },
  {
    "day": "2016-11-25",
    "value": 94
  },
  {
    "day": "2016-08-07",
    "value": 115
  },
  {
    "day": "2017-09-07",
    "value": 166
  },
  {
    "day": "2015-09-30",
    "value": 269
  },
  {
    "day": "2015-06-15",
    "value": 356
  },
  {
    "day": "2017-03-05",
    "value": 128
  },
  {
    "day": "2016-09-25",
    "value": 105
  },
  {
    "day": "2015-11-05",
    "value": 154
  },
  {
    "day": "2016-07-24",
    "value": 204
  },
  {
    "day": "2016-10-05",
    "value": 310
  },
  {
    "day": "2016-08-17",
    "value": 22
  },
  {
    "day": "2015-06-13",
    "value": 41
  },
  {
    "day": "2016-06-13",
    "value": 263
  },
  {
    "day": "2017-04-20",
    "value": 325
  },
  {
    "day": "2015-07-19",
    "value": 105
  },
  {
    "day": "2016-12-12",
    "value": 160
  },
  {
    "day": "2016-02-07",
    "value": 253
  },
  {
    "day": "2015-08-20",
    "value": 327
  },
  {
    "day": "2015-11-23",
    "value": 31
  },
  {
    "day": "2016-01-12",
    "value": 39
  },
  {
    "day": "2017-06-28",
    "value": 375
  },
  {
    "day": "2015-07-03",
    "value": 360
  },
  {
    "day": "2017-08-24",
    "value": 101
  },
  {
    "day": "2018-07-30",
    "value": 163
  },
  {
    "day": "2016-04-08",
    "value": 50
  },
  {
    "day": "2015-11-30",
    "value": 54
  },
  {
    "day": "2015-12-07",
    "value": 344
  },
  {
    "day": "2016-12-21",
    "value": 369
  },
  {
    "day": "2016-04-12",
    "value": 371
  },
  {
    "day": "2018-07-11",
    "value": 375
  },
  {
    "day": "2018-03-28",
    "value": 182
  },
  {
    "day": "2017-07-17",
    "value": 332
  },
  {
    "day": "2017-01-06",
    "value": 259
  },
  {
    "day": "2016-05-27",
    "value": 100
  },
  {
    "day": "2017-03-13",
    "value": 278
  },
  {
    "day": "2015-07-04",
    "value": 111
  },
  {
    "day": "2015-04-30",
    "value": 225
  },
  {
    "day": "2015-07-30",
    "value": 13
  },
  {
    "day": "2016-07-29",
    "value": 28
  },
  {
    "day": "2016-06-29",
    "value": 235
  },
  {
    "day": "2015-10-27",
    "value": 144
  },
  {
    "day": "2016-04-30",
    "value": 351
  },
  {
    "day": "2018-03-11",
    "value": 238
  },
  {
    "day": "2017-07-31",
    "value": 229
  },
  {
    "day": "2017-03-15",
    "value": 358
  },
  {
    "day": "2016-11-16",
    "value": 125
  },
  {
    "day": "2017-06-04",
    "value": 208
  },
  {
    "day": "2018-03-21",
    "value": 330
  },
  {
    "day": "2016-05-02",
    "value": 71
  },
  {
    "day": "2018-01-19",
    "value": 348
  },
  {
    "day": "2018-02-09",
    "value": 246
  },
  {
    "day": "2015-11-20",
    "value": 260
  },
  {
    "day": "2018-03-24",
    "value": 43
  },
  {
    "day": "2016-04-02",
    "value": 221
  },
  {
    "day": "2018-06-29",
    "value": 17
  },
  {
    "day": "2017-09-15",
    "value": 302
  },
  {
    "day": "2017-03-25",
    "value": 25
  },
  {
    "day": "2018-04-22",
    "value": 244
  },
  {
    "day": "2016-03-01",
    "value": 49
  },
  {
    "day": "2018-02-01",
    "value": 298
  },
  {
    "day": "2018-08-11",
    "value": 317
  },
  {
    "day": "2017-10-05",
    "value": 83
  },
  {
    "day": "2017-11-08",
    "value": 381
  },
  {
    "day": "2016-06-23",
    "value": 174
  },
  {
    "day": "2015-04-29",
    "value": 17
  },
  {
    "day": "2016-04-26",
    "value": 349
  },
  {
    "day": "2015-06-22",
    "value": 201
  },
  {
    "day": "2018-07-23",
    "value": 325
  },
  {
    "day": "2015-04-01",
    "value": 357
  },
  {
    "day": "2018-04-08",
    "value": 67
  },
  {
    "day": "2015-04-20",
    "value": 354
  },
  {
    "day": "2017-12-28",
    "value": 259
  },
  {
    "day": "2018-02-10",
    "value": 47
  },
  {
    "day": "2017-06-24",
    "value": 88
  },
  {
    "day": "2015-04-24",
    "value": 67
  },
  {
    "day": "2015-12-08",
    "value": 160
  },
  {
    "day": "2016-06-01",
    "value": 317
  },
  {
    "day": "2015-10-07",
    "value": 321
  },
  {
    "day": "2015-09-20",
    "value": 106
  },
  {
    "day": "2017-06-05",
    "value": 142
  },
  {
    "day": "2018-02-05",
    "value": 270
  },
  {
    "day": "2018-01-03",
    "value": 227
  },
  {
    "day": "2015-06-09",
    "value": 298
  },
  {
    "day": "2018-03-16",
    "value": 298
  },
  {
    "day": "2018-05-02",
    "value": 224
  },
  {
    "day": "2016-10-23",
    "value": 100
  },
  {
    "day": "2016-09-15",
    "value": 41
  },
  {
    "day": "2016-10-18",
    "value": 114
  },
  {
    "day": "2016-09-04",
    "value": 268
  },
  {
    "day": "2017-09-19",
    "value": 42
  },
  {
    "day": "2015-05-25",
    "value": 220
  },
  {
    "day": "2017-05-21",
    "value": 336
  },
  {
    "day": "2017-01-03",
    "value": 264
  },
  {
    "day": "2018-01-29",
    "value": 233
  },
  {
    "day": "2015-08-24",
    "value": 308
  },
  {
    "day": "2016-04-11",
    "value": 17
  },
  {
    "day": "2017-03-26",
    "value": 141
  },
  {
    "day": "2017-10-02",
    "value": 262
  },
  {
    "day": "2017-07-11",
    "value": 13
  },
  {
    "day": "2017-06-10",
    "value": 224
  },
  {
    "day": "2017-12-26",
    "value": 77
  },
  {
    "day": "2017-02-13",
    "value": 43
  },
  {
    "day": "2015-08-13",
    "value": 137
  },
  {
    "day": "2016-07-08",
    "value": 347
  },
  {
    "day": "2015-07-18",
    "value": 141
  },
  {
    "day": "2017-12-19",
    "value": 212
  },
  {
    "day": "2017-07-14",
    "value": 107
  },
  {
    "day": "2018-04-24",
    "value": 377
  },
  {
    "day": "2017-11-05",
    "value": 258
  },
  {
    "day": "2017-07-01",
    "value": 359
  },
  {
    "day": "2015-05-18",
    "value": 19
  },
  {
    "day": "2016-08-16",
    "value": 40
  },
  {
    "day": "2016-12-02",
    "value": 340
  },
  {
    "day": "2017-12-07",
    "value": 260
  },
  {
    "day": "2016-05-29",
    "value": 226
  },
  {
    "day": "2016-05-15",
    "value": 374
  },
  {
    "day": "2015-06-26",
    "value": 68
  },
  {
    "day": "2016-01-26",
    "value": 177
  },
  {
    "day": "2017-06-11",
    "value": 50
  },
  {
    "day": "2017-08-19",
    "value": 101
  },
  {
    "day": "2017-11-27",
    "value": 181
  },
  {
    "day": "2016-02-28",
    "value": 38
  },
  {
    "day": "2015-04-10",
    "value": 19
  },
  {
    "day": "2017-04-28",
    "value": 288
  },
  {
    "day": "2017-05-10",
    "value": 156
  },
  {
    "day": "2016-10-13",
    "value": 381
  },
  {
    "day": "2015-05-02",
    "value": 362
  },
  {
    "day": "2016-01-02",
    "value": 251
  },
  {
    "day": "2015-11-13",
    "value": 53
  },
  {
    "day": "2018-01-01",
    "value": 221
  },
  {
    "day": "2015-05-21",
    "value": 181
  },
  {
    "day": "2018-02-08",
    "value": 76
  },
  {
    "day": "2015-04-14",
    "value": 137
  },
  {
    "day": "2016-12-24",
    "value": 334
  },
  {
    "day": "2015-08-10",
    "value": 347
  },
  {
    "day": "2017-03-16",
    "value": 30
  },
  {
    "day": "2018-01-21",
    "value": 227
  },
  {
    "day": "2018-04-07",
    "value": 238
  },
  {
    "day": "2018-05-03",
    "value": 337
  },
  {
    "day": "2016-10-19",
    "value": 355
  },
  {
    "day": "2017-09-05",
    "value": 144
  },
  {
    "day": "2015-06-28",
    "value": 157
  },
  {
    "day": "2016-03-02",
    "value": 257
  },
  {
    "day": "2016-04-27",
    "value": 250
  },
  {
    "day": "2017-09-08",
    "value": 335
  },
  {
    "day": "2017-10-26",
    "value": 361
  },
  {
    "day": "2018-06-05",
    "value": 310
  },
  {
    "day": "2015-05-08",
    "value": 185
  },
  {
    "day": "2015-11-26",
    "value": 328
  },
  {
    "day": "2015-07-26",
    "value": 393
  },
  {
    "day": "2016-03-14",
    "value": 160
  },
  {
    "day": "2016-08-03",
    "value": 250
  },
  {
    "day": "2015-10-02",
    "value": 81
  },
  {
    "day": "2017-03-21",
    "value": 51
  },
  {
    "day": "2018-07-15",
    "value": 249
  },
  {
    "day": "2015-09-04",
    "value": 168
  },
  {
    "day": "2017-12-04",
    "value": 139
  },
  {
    "day": "2018-05-30",
    "value": 98
  },
  {
    "day": "2016-02-11",
    "value": 166
  },
  {
    "day": "2015-05-26",
    "value": 314
  },
  {
    "day": "2015-12-27",
    "value": 98
  },
  {
    "day": "2017-03-19",
    "value": 131
  },
  {
    "day": "2016-01-29",
    "value": 111
  },
  {
    "day": "2018-04-06",
    "value": 397
  },
  {
    "day": "2016-11-30",
    "value": 216
  },
  {
    "day": "2015-11-07",
    "value": 210
  },
  {
    "day": "2016-06-05",
    "value": 355
  },
  {
    "day": "2016-07-06",
    "value": 370
  },
  {
    "day": "2018-05-01",
    "value": 166
  },
  {
    "day": "2016-11-20",
    "value": 119
  },
  {
    "day": "2015-04-22",
    "value": 280
  },
  {
    "day": "2018-06-27",
    "value": 389
  },
  {
    "day": "2015-04-23",
    "value": 389
  },
  {
    "day": "2016-01-20",
    "value": 38
  },
  {
    "day": "2017-02-21",
    "value": 36
  },
  {
    "day": "2017-01-01",
    "value": 396
  },
  {
    "day": "2016-06-03",
    "value": 218
  },
  {
    "day": "2016-08-15",
    "value": 375
  },
  {
    "day": "2018-05-27",
    "value": 380
  },
  {
    "day": "2016-09-02",
    "value": 3
  },
  {
    "day": "2017-01-19",
    "value": 197
  },
  {
    "day": "2016-04-15",
    "value": 342
  },
  {
    "day": "2016-11-03",
    "value": 357
  },
  {
    "day": "2017-08-15",
    "value": 238
  },
  {
    "day": "2018-08-06",
    "value": 328
  },
  {
    "day": "2016-01-13",
    "value": 206
  },
  {
    "day": "2015-07-28",
    "value": 389
  },
  {
    "day": "2016-05-05",
    "value": 153
  },
  {
    "day": "2018-02-07",
    "value": 350
  },
  {
    "day": "2017-05-26",
    "value": 259
  },
  {
    "day": "2017-05-29",
    "value": 32
  },
  {
    "day": "2017-03-24",
    "value": 33
  },
  {
    "day": "2017-07-07",
    "value": 261
  },
  {
    "day": "2018-06-16",
    "value": 258
  },
  {
    "day": "2016-10-28",
    "value": 156
  },
  {
    "day": "2016-11-15",
    "value": 36
  },
  {
    "day": "2017-05-02",
    "value": 54
  },
  {
    "day": "2016-01-21",
    "value": 327
  },
  {
    "day": "2015-10-03",
    "value": 112
  },
  {
    "day": "2015-09-21",
    "value": 347
  },
  {
    "day": "2016-09-03",
    "value": 34
  },
  {
    "day": "2018-03-18",
    "value": 204
  },
  {
    "day": "2016-03-28",
    "value": 296
  },
  {
    "day": "2018-02-04",
    "value": 310
  },
  {
    "day": "2017-09-01",
    "value": 40
  },
  {
    "day": "2017-01-17",
    "value": 386
  },
  {
    "day": "2018-03-26",
    "value": 188
  },
  {
    "day": "2017-10-07",
    "value": 212
  },
  {
    "day": "2017-05-09",
    "value": 235
  },
  {
    "day": "2017-11-04",
    "value": 69
  },
  {
    "day": "2017-08-10",
    "value": 274
  },
  {
    "day": "2017-03-23",
    "value": 86
  },
  {
    "day": "2016-12-19",
    "value": 349
  },
  {
    "day": "2018-02-14",
    "value": 323
  },
  {
    "day": "2018-07-29",
    "value": 152
  },
  {
    "day": "2017-02-17",
    "value": 346
  },
  {
    "day": "2017-03-01",
    "value": 113
  },
  {
    "day": "2017-01-07",
    "value": 98
  },
  {
    "day": "2017-02-22",
    "value": 368
  },
  {
    "day": "2018-03-06",
    "value": 220
  },
  {
    "day": "2017-03-02",
    "value": 248
  },
  {
    "day": "2016-05-24",
    "value": 313
  },
  {
    "day": "2018-05-31",
    "value": 9
  },
  {
    "day": "2017-04-25",
    "value": 162
  },
  {
    "day": "2015-07-14",
    "value": 394
  },
  {
    "day": "2017-01-25",
    "value": 388
  },
  {
    "day": "2017-04-26",
    "value": 316
  },
  {
    "day": "2016-12-29",
    "value": 166
  },
  {
    "day": "2016-05-17",
    "value": 365
  },
  {
    "day": "2016-08-11",
    "value": 78
  },
  {
    "day": "2016-01-04",
    "value": 184
  },
  {
    "day": "2017-08-07",
    "value": 189
  },
  {
    "day": "2018-06-30",
    "value": 96
  },
  {
    "day": "2017-05-31",
    "value": 157
  },
  {
    "day": "2016-07-19",
    "value": 13
  },
  {
    "day": "2016-03-15",
    "value": 154
  },
  {
    "day": "2018-07-21",
    "value": 305
  },
  {
    "day": "2016-06-22",
    "value": 322
  },
  {
    "day": "2017-09-25",
    "value": 160
  },
  {
    "day": "2018-03-02",
    "value": 227
  },
  {
    "day": "2016-02-24",
    "value": 51
  },
  {
    "day": "2017-10-21",
    "value": 335
  },
  {
    "day": "2017-05-11",
    "value": 149
  },
  {
    "day": "2015-09-01",
    "value": 355
  },
  {
    "day": "2017-09-26",
    "value": 122
  },
  {
    "day": "2017-11-13",
    "value": 288
  },
  {
    "day": "2017-04-17",
    "value": 171
  },
  {
    "day": "2016-08-01",
    "value": 239
  },
  {
    "day": "2017-11-23",
    "value": 330
  },
  {
    "day": "2017-08-02",
    "value": 150
  },
  {
    "day": "2017-07-22",
    "value": 3
  },
  {
    "day": "2015-09-15",
    "value": 385
  },
  {
    "day": "2016-07-12",
    "value": 233
  },
  {
    "day": "2015-07-05",
    "value": 114
  },
  {
    "day": "2017-05-04",
    "value": 154
  },
  {
    "day": "2017-10-22",
    "value": 354
  },
  {
    "day": "2015-07-02",
    "value": 161
  },
  {
    "day": "2018-05-08",
    "value": 24
  },
  {
    "day": "2016-10-10",
    "value": 102
  },
  {
    "day": "2015-05-28",
    "value": 357
  },
  {
    "day": "2018-04-19",
    "value": 176
  },
  {
    "day": "2015-11-18",
    "value": 150
  },
  {
    "day": "2016-01-05",
    "value": 184
  },
  {
    "day": "2016-11-24",
    "value": 185
  },
  {
    "day": "2016-02-08",
    "value": 274
  },
  {
    "day": "2018-06-06",
    "value": 2
  },
  {
    "day": "2015-04-19",
    "value": 269
  },
  {
    "day": "2017-08-17",
    "value": 237
  },
  {
    "day": "2018-02-20",
    "value": 208
  },
  {
    "day": "2016-12-18",
    "value": 3
  },
  {
    "day": "2016-11-11",
    "value": 223
  }
]
return (
  <DatavizCardFull
    title="A heatmap demo"
    dataviz={<CoSoCalendar data={data} />}
    unique_id="SwarmPlotDemo"
    loading={loading}
  >
    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
  </DatavizCardFull>
);
}

export default CalendarDemo;
