import CoSoScatterPlot from "../ScatterPlot"
import {DatavizCardFull} from "../../DatavizCards";
import { useState } from "react";

function ScatterPlotDemo(props) {
  const [loading, setLoading] = useState(true);

  setTimeout(() => {
    setLoading(false)
  }, 4000)

  const data = [
    {
      "id": "group A",
      "data": [
        {
          "x": 86,
          "y": 59
        },
        {
          "x": 7,
          "y": 110
        },
        {
          "x": 13,
          "y": 79
        },
        {
          "x": 79,
          "y": 114
        },
        {
          "x": 19,
          "y": 48
        },
        {
          "x": 48,
          "y": 93
        },
        {
          "x": 100,
          "y": 120
        },
        {
          "x": 71,
          "y": 101
        },
        {
          "x": 86,
          "y": 41
        },
        {
          "x": 90,
          "y": 60
        },
        {
          "x": 53,
          "y": 65
        },
        {
          "x": 15,
          "y": 112
        },
        {
          "x": 70,
          "y": 0
        },
        {
          "x": 43,
          "y": 12
        },
        {
          "x": 39,
          "y": 77
        },
        {
          "x": 43,
          "y": 48
        },
        {
          "x": 20,
          "y": 17
        },
        {
          "x": 6,
          "y": 21
        },
        {
          "x": 34,
          "y": 101
        },
        {
          "x": 7,
          "y": 22
        },
        {
          "x": 2,
          "y": 20
        },
        {
          "x": 96,
          "y": 87
        },
        {
          "x": 47,
          "y": 71
        },
        {
          "x": 57,
          "y": 108
        },
        {
          "x": 69,
          "y": 99
        },
        {
          "x": 5,
          "y": 45
        },
        {
          "x": 88,
          "y": 72
        },
        {
          "x": 94,
          "y": 65
        },
        {
          "x": 12,
          "y": 78
        },
        {
          "x": 61,
          "y": 109
        },
        {
          "x": 24,
          "y": 116
        },
        {
          "x": 33,
          "y": 114
        },
        {
          "x": 60,
          "y": 49
        },
        {
          "x": 78,
          "y": 35
        },
        {
          "x": 24,
          "y": 4
        },
        {
          "x": 77,
          "y": 59
        },
        {
          "x": 64,
          "y": 77
        },
        {
          "x": 41,
          "y": 0
        },
        {
          "x": 33,
          "y": 4
        },
        {
          "x": 16,
          "y": 99
        },
        {
          "x": 80,
          "y": 106
        },
        {
          "x": 78,
          "y": 117
        },
        {
          "x": 85,
          "y": 57
        },
        {
          "x": 98,
          "y": 82
        },
        {
          "x": 66,
          "y": 51
        },
        {
          "x": 76,
          "y": 113
        },
        {
          "x": 29,
          "y": 97
        },
        {
          "x": 67,
          "y": 27
        },
        {
          "x": 59,
          "y": 7
        },
        {
          "x": 61,
          "y": 113
        }
      ]
    },
    {
      "id": "group B",
      "data": [
        {
          "x": 64,
          "y": 71
        },
        {
          "x": 85,
          "y": 84
        },
        {
          "x": 89,
          "y": 107
        },
        {
          "x": 16,
          "y": 51
        },
        {
          "x": 87,
          "y": 87
        },
        {
          "x": 40,
          "y": 8
        },
        {
          "x": 30,
          "y": 69
        },
        {
          "x": 98,
          "y": 118
        },
        {
          "x": 19,
          "y": 4
        },
        {
          "x": 54,
          "y": 83
        },
        {
          "x": 25,
          "y": 110
        },
        {
          "x": 86,
          "y": 70
        },
        {
          "x": 85,
          "y": 90
        },
        {
          "x": 92,
          "y": 28
        },
        {
          "x": 67,
          "y": 108
        },
        {
          "x": 100,
          "y": 56
        },
        {
          "x": 18,
          "y": 76
        },
        {
          "x": 58,
          "y": 87
        },
        {
          "x": 62,
          "y": 7
        },
        {
          "x": 76,
          "y": 117
        },
        {
          "x": 45,
          "y": 70
        },
        {
          "x": 94,
          "y": 81
        },
        {
          "x": 64,
          "y": 39
        },
        {
          "x": 4,
          "y": 77
        },
        {
          "x": 5,
          "y": 65
        },
        {
          "x": 29,
          "y": 14
        },
        {
          "x": 31,
          "y": 71
        },
        {
          "x": 84,
          "y": 99
        },
        {
          "x": 68,
          "y": 64
        },
        {
          "x": 63,
          "y": 98
        },
        {
          "x": 3,
          "y": 110
        },
        {
          "x": 45,
          "y": 96
        },
        {
          "x": 82,
          "y": 42
        },
        {
          "x": 93,
          "y": 26
        },
        {
          "x": 2,
          "y": 89
        },
        {
          "x": 70,
          "y": 13
        },
        {
          "x": 67,
          "y": 118
        },
        {
          "x": 43,
          "y": 45
        },
        {
          "x": 79,
          "y": 67
        },
        {
          "x": 99,
          "y": 0
        },
        {
          "x": 0,
          "y": 1
        },
        {
          "x": 78,
          "y": 110
        },
        {
          "x": 35,
          "y": 100
        },
        {
          "x": 42,
          "y": 71
        },
        {
          "x": 88,
          "y": 68
        },
        {
          "x": 47,
          "y": 61
        },
        {
          "x": 84,
          "y": 0
        },
        {
          "x": 20,
          "y": 87
        },
        {
          "x": 4,
          "y": 48
        },
        {
          "x": 37,
          "y": 48
        }
      ]
    },
    {
      "id": "group C",
      "data": [
        {
          "x": 74,
          "y": 100
        },
        {
          "x": 72,
          "y": 24
        },
        {
          "x": 5,
          "y": 96
        },
        {
          "x": 21,
          "y": 107
        },
        {
          "x": 84,
          "y": 101
        },
        {
          "x": 79,
          "y": 114
        },
        {
          "x": 76,
          "y": 3
        },
        {
          "x": 1,
          "y": 70
        },
        {
          "x": 53,
          "y": 41
        },
        {
          "x": 70,
          "y": 55
        },
        {
          "x": 60,
          "y": 26
        },
        {
          "x": 60,
          "y": 119
        },
        {
          "x": 20,
          "y": 38
        },
        {
          "x": 65,
          "y": 96
        },
        {
          "x": 9,
          "y": 111
        },
        {
          "x": 21,
          "y": 63
        },
        {
          "x": 87,
          "y": 78
        },
        {
          "x": 61,
          "y": 29
        },
        {
          "x": 43,
          "y": 2
        },
        {
          "x": 78,
          "y": 47
        },
        {
          "x": 41,
          "y": 95
        },
        {
          "x": 73,
          "y": 17
        },
        {
          "x": 54,
          "y": 83
        },
        {
          "x": 66,
          "y": 116
        },
        {
          "x": 90,
          "y": 40
        },
        {
          "x": 89,
          "y": 7
        },
        {
          "x": 1,
          "y": 36
        },
        {
          "x": 10,
          "y": 14
        },
        {
          "x": 57,
          "y": 56
        },
        {
          "x": 2,
          "y": 40
        },
        {
          "x": 19,
          "y": 29
        },
        {
          "x": 41,
          "y": 86
        },
        {
          "x": 64,
          "y": 32
        },
        {
          "x": 76,
          "y": 5
        },
        {
          "x": 42,
          "y": 82
        },
        {
          "x": 75,
          "y": 62
        },
        {
          "x": 61,
          "y": 10
        },
        {
          "x": 52,
          "y": 74
        },
        {
          "x": 62,
          "y": 52
        },
        {
          "x": 17,
          "y": 100
        },
        {
          "x": 51,
          "y": 96
        },
        {
          "x": 77,
          "y": 99
        },
        {
          "x": 33,
          "y": 36
        },
        {
          "x": 37,
          "y": 5
        },
        {
          "x": 70,
          "y": 66
        },
        {
          "x": 59,
          "y": 106
        },
        {
          "x": 28,
          "y": 28
        },
        {
          "x": 8,
          "y": 44
        },
        {
          "x": 53,
          "y": 69
        },
        {
          "x": 20,
          "y": 1
        }
      ]
    },
    {
      "id": "group D",
      "data": [
        {
          "x": 40,
          "y": 32
        },
        {
          "x": 14,
          "y": 76
        },
        {
          "x": 58,
          "y": 82
        },
        {
          "x": 51,
          "y": 105
        },
        {
          "x": 36,
          "y": 82
        },
        {
          "x": 37,
          "y": 47
        },
        {
          "x": 74,
          "y": 1
        },
        {
          "x": 0,
          "y": 43
        },
        {
          "x": 29,
          "y": 7
        },
        {
          "x": 85,
          "y": 20
        },
        {
          "x": 7,
          "y": 1
        },
        {
          "x": 90,
          "y": 105
        },
        {
          "x": 75,
          "y": 52
        },
        {
          "x": 12,
          "y": 103
        },
        {
          "x": 22,
          "y": 91
        },
        {
          "x": 73,
          "y": 38
        },
        {
          "x": 13,
          "y": 17
        },
        {
          "x": 31,
          "y": 5
        },
        {
          "x": 89,
          "y": 58
        },
        {
          "x": 35,
          "y": 38
        },
        {
          "x": 80,
          "y": 32
        },
        {
          "x": 97,
          "y": 19
        },
        {
          "x": 62,
          "y": 12
        },
        {
          "x": 73,
          "y": 70
        },
        {
          "x": 2,
          "y": 115
        },
        {
          "x": 74,
          "y": 66
        },
        {
          "x": 75,
          "y": 28
        },
        {
          "x": 44,
          "y": 99
        },
        {
          "x": 6,
          "y": 14
        },
        {
          "x": 85,
          "y": 65
        },
        {
          "x": 9,
          "y": 50
        },
        {
          "x": 63,
          "y": 29
        },
        {
          "x": 66,
          "y": 38
        },
        {
          "x": 49,
          "y": 83
        },
        {
          "x": 75,
          "y": 60
        },
        {
          "x": 28,
          "y": 87
        },
        {
          "x": 92,
          "y": 116
        },
        {
          "x": 2,
          "y": 116
        },
        {
          "x": 16,
          "y": 1
        },
        {
          "x": 63,
          "y": 8
        },
        {
          "x": 25,
          "y": 114
        },
        {
          "x": 23,
          "y": 100
        },
        {
          "x": 93,
          "y": 57
        },
        {
          "x": 38,
          "y": 103
        },
        {
          "x": 15,
          "y": 66
        },
        {
          "x": 71,
          "y": 70
        },
        {
          "x": 23,
          "y": 31
        },
        {
          "x": 25,
          "y": 46
        },
        {
          "x": 97,
          "y": 30
        },
        {
          "x": 77,
          "y": 86
        }
      ]
    },
    {
      "id": "group E",
      "data": [
        {
          "x": 96,
          "y": 78
        },
        {
          "x": 29,
          "y": 69
        },
        {
          "x": 84,
          "y": 37
        },
        {
          "x": 100,
          "y": 26
        },
        {
          "x": 28,
          "y": 66
        },
        {
          "x": 69,
          "y": 113
        },
        {
          "x": 34,
          "y": 41
        },
        {
          "x": 67,
          "y": 8
        },
        {
          "x": 23,
          "y": 45
        },
        {
          "x": 17,
          "y": 98
        },
        {
          "x": 1,
          "y": 75
        },
        {
          "x": 65,
          "y": 38
        },
        {
          "x": 7,
          "y": 101
        },
        {
          "x": 62,
          "y": 33
        },
        {
          "x": 65,
          "y": 45
        },
        {
          "x": 98,
          "y": 9
        },
        {
          "x": 48,
          "y": 69
        },
        {
          "x": 83,
          "y": 43
        },
        {
          "x": 39,
          "y": 77
        },
        {
          "x": 68,
          "y": 8
        },
        {
          "x": 81,
          "y": 105
        },
        {
          "x": 57,
          "y": 26
        },
        {
          "x": 24,
          "y": 44
        },
        {
          "x": 54,
          "y": 1
        },
        {
          "x": 43,
          "y": 27
        },
        {
          "x": 88,
          "y": 20
        },
        {
          "x": 75,
          "y": 99
        },
        {
          "x": 7,
          "y": 101
        },
        {
          "x": 7,
          "y": 92
        },
        {
          "x": 15,
          "y": 43
        },
        {
          "x": 28,
          "y": 84
        },
        {
          "x": 97,
          "y": 41
        },
        {
          "x": 16,
          "y": 49
        },
        {
          "x": 24,
          "y": 64
        },
        {
          "x": 8,
          "y": 77
        },
        {
          "x": 88,
          "y": 52
        },
        {
          "x": 16,
          "y": 50
        },
        {
          "x": 49,
          "y": 99
        },
        {
          "x": 8,
          "y": 96
        },
        {
          "x": 68,
          "y": 14
        },
        {
          "x": 99,
          "y": 66
        },
        {
          "x": 77,
          "y": 105
        },
        {
          "x": 79,
          "y": 30
        },
        {
          "x": 76,
          "y": 43
        },
        {
          "x": 73,
          "y": 106
        },
        {
          "x": 31,
          "y": 107
        },
        {
          "x": 22,
          "y": 58
        },
        {
          "x": 54,
          "y": 8
        },
        {
          "x": 58,
          "y": 31
        },
        {
          "x": 18,
          "y": 23
        }
      ]
    }
  ]
  return (
    <DatavizCardFull
      title="A heatmap demo"
      dataviz={<CoSoScatterPlot data={data} />}
      unique_id="SwarmPlotDemo"
      loading={loading}
    >
      Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
    </DatavizCardFull>
  );
}

export default ScatterPlotDemo;
