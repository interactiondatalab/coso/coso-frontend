import BarTaskFrequency from "../BarTaskFrequency"
import {useContext, useState, useEffect} from 'react'
import {UserContext} from "../../../utils/UserContext"
import {DatavizCardFull} from "../../DatavizCards";

import Api from "../../../Api"

function BarTaskFrequencyDemo(props) {
  const [data, setData] = useState([])
  const [loading, setLoading] = useState(true)
  const [refresh, setRefresh] = useState(false)

  setTimeout(() => {
    setLoading(false)
  }, 3000)

  useEffect(() => {
    if (data.length === 0) {
      // Here we define our API call
      // THIS IS IMPORTANT
      // The url: "slack/get_reactions_count" corresponds to:
      // http://the.backend.url/api/slack/get_reactions_count
      // and it will also send the correct credentials for you
      // basically, you only have to ask for the correct API endpoint and
      // you will receive the data back
      Api.get("stats/logs_frequency")
      .then(res => {
        // we receive the data and we do something with it
        console.log(res.data)

        // here we take the the "reactions" array and save it into our State: data
        setData(res.data.sort(function(a,b) { return a.count <= b.count }));
        // After we set data, react will then -> Re-Render the dataviz
        // because the dataviz DEPENDS on data, and data is a state

        // Then we set the loading state to false
        setLoading(false);
        // This will remove the loading icon
      })
    }
  }, [data])


  // This is so that the refresh removes the data
  useEffect(() => {
    setData([])
  }, [refresh])


  return (
    <DatavizCardFull
      title="Example BarTaskFrequency" // Set a fancy title
      dataviz={<BarTaskFrequency data={data}/>} // Here, you are sending the Calendar we have created above !!
      unique_id="BarTaskFrequencydemo" // THIS NEEDS TO BE UNIQUE !!! If it is not, then the download button will fail
      loading={loading}
      height={500}
    >
    </DatavizCardFull>
  )
}

// =============================================================================
// ADD IT TO THE INSIGHTS
// go to the /pages/Insights.js

export default BarTaskFrequencyDemo;
