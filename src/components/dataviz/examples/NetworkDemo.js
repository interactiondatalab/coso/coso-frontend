import CoSoNetwork from "../Network"
import {DatavizCardSplit} from "../../DatavizCards";
import { useState } from "react";

function NetworkDemo(props) {
  const [loading, setLoading] = useState(true);

  setTimeout(() => {
    setLoading(false)
  }, 5500)

  const data = {
  "nodes": [
    {
      "id": "1",
      "radius": 8,
      "depth": 1,
      "color": "rgb(97, 205, 187)"
    },
    {
      "id": "2",
      "radius": 8,
      "depth": 1,
      "color": "rgb(97, 205, 187)"
    },
    {
      "id": "3",
      "radius": 8,
      "depth": 1,
      "color": "rgb(97, 205, 187)"
    },
    {
      "id": "4",
      "radius": 8,
      "depth": 1,
      "color": "rgb(97, 205, 187)"
    },
    {
      "id": "5",
      "radius": 8,
      "depth": 1,
      "color": "rgb(97, 205, 187)"
    },
    {
      "id": "6",
      "radius": 8,
      "depth": 1,
      "color": "rgb(97, 205, 187)"
    },
    {
      "id": "7",
      "radius": 8,
      "depth": 1,
      "color": "rgb(97, 205, 187)"
    },
    {
      "id": "8",
      "radius": 8,
      "depth": 1,
      "color": "rgb(97, 205, 187)"
    },
    {
      "id": "9",
      "radius": 8,
      "depth": 1,
      "color": "rgb(97, 205, 187)"
    },
    {
      "id": "0",
      "radius": 12,
      "depth": 0,
      "color": "rgb(244, 117, 96)"
    },
    {
      "id": "1.0",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "1.1",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "1.2",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "1.3",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "1.4",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "1.5",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "1.6",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "1.7",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "2.0",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "2.1",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "2.2",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "2.3",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "2.4",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "2.5",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "2.6",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "2.7",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "2.8",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "2.9",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "2.10",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "2.11",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "2.12",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "2.13",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "2.14",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "2.15",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "3.0",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "3.1",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "3.2",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "3.3",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "3.4",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "3.5",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "4.0",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "4.1",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "4.2",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "4.3",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "4.4",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "4.5",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "4.6",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "4.7",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "4.8",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "4.9",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "4.10",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "5.0",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "5.1",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "5.2",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "5.3",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "5.4",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "5.5",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "5.6",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "5.7",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "6.0",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "6.1",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "6.2",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "6.3",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "6.4",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "6.5",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "6.6",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "7.0",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "7.1",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "7.2",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "7.3",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "7.4",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "7.5",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "7.6",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "7.7",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "7.8",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "7.9",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "7.10",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "7.11",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "7.12",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "8.0",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "8.1",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "8.2",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "8.3",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "8.4",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "8.5",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "8.6",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "8.7",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "8.8",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "8.9",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "8.10",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "8.11",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "8.12",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "8.13",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "9.0",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "9.1",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "9.2",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "9.3",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "9.4",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "9.5",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "9.6",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "9.7",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "9.8",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "9.9",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "9.10",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "9.11",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "9.12",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "9.13",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    },
    {
      "id": "9.14",
      "radius": 4,
      "depth": 2,
      "color": "rgb(232, 193, 160)"
    }
  ],
  "links": [
    {
      "source": "0",
      "target": "1",
      "distance": 50
    },
    {
      "source": "1",
      "target": "1.0",
      "distance": 30
    },
    {
      "source": "1",
      "target": "1.1",
      "distance": 30
    },
    {
      "source": "1",
      "target": "1.2",
      "distance": 30
    },
    {
      "source": "1",
      "target": "1.3",
      "distance": 30
    },
    {
      "source": "1",
      "target": "1.4",
      "distance": 30
    },
    {
      "source": "1",
      "target": "1.5",
      "distance": 30
    },
    {
      "source": "1",
      "target": "1.6",
      "distance": 30
    },
    {
      "source": "1",
      "target": "1.7",
      "distance": 30
    },
    {
      "source": "0",
      "target": "2",
      "distance": 50
    },
    {
      "source": "2",
      "target": "4",
      "distance": 70
    },
    {
      "source": "2",
      "target": "2.0",
      "distance": 30
    },
    {
      "source": "2",
      "target": "2.1",
      "distance": 30
    },
    {
      "source": "2",
      "target": "2.2",
      "distance": 30
    },
    {
      "source": "2",
      "target": "2.3",
      "distance": 30
    },
    {
      "source": "2",
      "target": "2.4",
      "distance": 30
    },
    {
      "source": "2",
      "target": "2.5",
      "distance": 30
    },
    {
      "source": "2",
      "target": "2.6",
      "distance": 30
    },
    {
      "source": "2",
      "target": "2.7",
      "distance": 30
    },
    {
      "source": "2",
      "target": "2.8",
      "distance": 30
    },
    {
      "source": "2",
      "target": "2.9",
      "distance": 30
    },
    {
      "source": "2",
      "target": "2.10",
      "distance": 30
    },
    {
      "source": "2",
      "target": "2.11",
      "distance": 30
    },
    {
      "source": "2",
      "target": "2.12",
      "distance": 30
    },
    {
      "source": "2",
      "target": "2.13",
      "distance": 30
    },
    {
      "source": "2",
      "target": "2.14",
      "distance": 30
    },
    {
      "source": "2",
      "target": "2.15",
      "distance": 30
    },
    {
      "source": "0",
      "target": "3",
      "distance": 50
    },
    {
      "source": "3",
      "target": "2",
      "distance": 70
    },
    {
      "source": "3",
      "target": "3.0",
      "distance": 30
    },
    {
      "source": "3",
      "target": "3.1",
      "distance": 30
    },
    {
      "source": "3",
      "target": "3.2",
      "distance": 30
    },
    {
      "source": "3",
      "target": "3.3",
      "distance": 30
    },
    {
      "source": "3",
      "target": "3.4",
      "distance": 30
    },
    {
      "source": "3",
      "target": "3.5",
      "distance": 30
    },
    {
      "source": "0",
      "target": "4",
      "distance": 50
    },
    {
      "source": "4",
      "target": "4.0",
      "distance": 30
    },
    {
      "source": "4",
      "target": "4.1",
      "distance": 30
    },
    {
      "source": "4",
      "target": "4.2",
      "distance": 30
    },
    {
      "source": "4",
      "target": "4.3",
      "distance": 30
    },
    {
      "source": "4",
      "target": "4.4",
      "distance": 30
    },
    {
      "source": "4",
      "target": "4.5",
      "distance": 30
    },
    {
      "source": "4",
      "target": "4.6",
      "distance": 30
    },
    {
      "source": "4",
      "target": "4.7",
      "distance": 30
    },
    {
      "source": "4",
      "target": "4.8",
      "distance": 30
    },
    {
      "source": "4",
      "target": "4.9",
      "distance": 30
    },
    {
      "source": "4",
      "target": "4.10",
      "distance": 30
    },
    {
      "source": "0",
      "target": "5",
      "distance": 50
    },
    {
      "source": "5",
      "target": "5.0",
      "distance": 30
    },
    {
      "source": "5",
      "target": "5.1",
      "distance": 30
    },
    {
      "source": "5",
      "target": "5.2",
      "distance": 30
    },
    {
      "source": "5",
      "target": "5.3",
      "distance": 30
    },
    {
      "source": "5",
      "target": "5.4",
      "distance": 30
    },
    {
      "source": "5",
      "target": "5.5",
      "distance": 30
    },
    {
      "source": "5",
      "target": "5.6",
      "distance": 30
    },
    {
      "source": "5",
      "target": "5.7",
      "distance": 30
    },
    {
      "source": "0",
      "target": "6",
      "distance": 50
    },
    {
      "source": "6",
      "target": "6",
      "distance": 70
    },
    {
      "source": "6",
      "target": "6.0",
      "distance": 30
    },
    {
      "source": "6",
      "target": "6.1",
      "distance": 30
    },
    {
      "source": "6",
      "target": "6.2",
      "distance": 30
    },
    {
      "source": "6",
      "target": "6.3",
      "distance": 30
    },
    {
      "source": "6",
      "target": "6.4",
      "distance": 30
    },
    {
      "source": "6",
      "target": "6.5",
      "distance": 30
    },
    {
      "source": "6",
      "target": "6.6",
      "distance": 30
    },
    {
      "source": "0",
      "target": "7",
      "distance": 50
    },
    {
      "source": "7",
      "target": "7.0",
      "distance": 30
    },
    {
      "source": "7",
      "target": "7.1",
      "distance": 30
    },
    {
      "source": "7",
      "target": "7.2",
      "distance": 30
    },
    {
      "source": "7",
      "target": "7.3",
      "distance": 30
    },
    {
      "source": "7",
      "target": "7.4",
      "distance": 30
    },
    {
      "source": "7",
      "target": "7.5",
      "distance": 30
    },
    {
      "source": "7",
      "target": "7.6",
      "distance": 30
    },
    {
      "source": "7",
      "target": "7.7",
      "distance": 30
    },
    {
      "source": "7",
      "target": "7.8",
      "distance": 30
    },
    {
      "source": "7",
      "target": "7.9",
      "distance": 30
    },
    {
      "source": "7",
      "target": "7.10",
      "distance": 30
    },
    {
      "source": "7",
      "target": "7.11",
      "distance": 30
    },
    {
      "source": "7",
      "target": "7.12",
      "distance": 30
    },
    {
      "source": "0",
      "target": "8",
      "distance": 50
    },
    {
      "source": "8",
      "target": "8.0",
      "distance": 30
    },
    {
      "source": "8",
      "target": "8.1",
      "distance": 30
    },
    {
      "source": "8",
      "target": "8.2",
      "distance": 30
    },
    {
      "source": "8",
      "target": "8.3",
      "distance": 30
    },
    {
      "source": "8",
      "target": "8.4",
      "distance": 30
    },
    {
      "source": "8",
      "target": "8.5",
      "distance": 30
    },
    {
      "source": "8",
      "target": "8.6",
      "distance": 30
    },
    {
      "source": "8",
      "target": "8.7",
      "distance": 30
    },
    {
      "source": "8",
      "target": "8.8",
      "distance": 30
    },
    {
      "source": "8",
      "target": "8.9",
      "distance": 30
    },
    {
      "source": "8",
      "target": "8.10",
      "distance": 30
    },
    {
      "source": "8",
      "target": "8.11",
      "distance": 30
    },
    {
      "source": "8",
      "target": "8.12",
      "distance": 30
    },
    {
      "source": "8",
      "target": "8.13",
      "distance": 30
    },
    {
      "source": "0",
      "target": "9",
      "distance": 50
    },
    {
      "source": "9",
      "target": "5",
      "distance": 70
    },
    {
      "source": "9",
      "target": "8",
      "distance": 70
    },
    {
      "source": "9",
      "target": "9.0",
      "distance": 30
    },
    {
      "source": "9",
      "target": "9.1",
      "distance": 30
    },
    {
      "source": "9",
      "target": "9.2",
      "distance": 30
    },
    {
      "source": "9",
      "target": "9.3",
      "distance": 30
    },
    {
      "source": "9",
      "target": "9.4",
      "distance": 30
    },
    {
      "source": "9",
      "target": "9.5",
      "distance": 30
    },
    {
      "source": "9",
      "target": "9.6",
      "distance": 30
    },
    {
      "source": "9",
      "target": "9.7",
      "distance": 30
    },
    {
      "source": "9",
      "target": "9.8",
      "distance": 30
    },
    {
      "source": "9",
      "target": "9.9",
      "distance": 30
    },
    {
      "source": "9",
      "target": "9.10",
      "distance": 30
    },
    {
      "source": "9",
      "target": "9.11",
      "distance": 30
    },
    {
      "source": "9",
      "target": "9.12",
      "distance": 30
    },
    {
      "source": "9",
      "target": "9.13",
      "distance": 30
    },
    {
      "source": "9",
      "target": "9.14",
      "distance": 30
    }
  ]
}
return (
  <DatavizCardSplit
    title="A heatmap demo"
    dataviz={<CoSoNetwork data={data} />}
    unique_id="SwarmPlotDemo"
    loading={loading}
  >
    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
  </DatavizCardSplit>
);
}

export default NetworkDemo;
