// =============================================================================
// IMPORTS

// First import the Dataviz module yoat u want to use
// For a full list of the available module refere to the Nivo Website
// Always use the == Responsive == version of the module !
import { ResponsiveCalendar } from '@nivo/calendar'

// We import the Api so that we can get the data from the Api ...
import Api from "../../Api"

// Then we import react components that we will need here
import {useContext, useState, useEffect} from 'react'
import {UserContext} from "../../utils/UserContext"
import {DatavizCardFull, DatavizCardSplit} from "../DatavizCards"


// =============================================================================
// Dataviz configuration

// Here we will define our dataviz
// For this example, we will use the Calendar from Nivo, and indeed this is what
// we have imported above !

// we define a Calendar function, that will take one parameter: data
// according to the Nivo documentation, data must look like this:

// Array<{
//     day:   string // format must be YYYY-MM-DD,
//     value: number
// }>

// https://nivo.rocks/calendar/

const Calendar = ({data}) => {
  // So as to not crash everything if our data is not yet loaded, we test
  // if data is empty then we return null
  if (data === []) {
    return (null)
  // else we return the Nivo calendar component
  } else {
    return (
      // What follows is taken from the nivo documentation
      // https://nivo.rocks/calendar/
      // If you want to know more, and test the different parameter
      // Go on the Nivo website as it is really well made

      <ResponsiveCalendar
            data={data}
            from="2018-03-01"
            to="2020-07-12"
            emptyColor="#eeeeee"
            colors={[ '#61cdbb', '#97e3d5', '#e8c1a0', '#f47560' ]}
            margin={{ top: 40, right: 40, bottom: 40, left: 40 }}
            yearSpacing={40}
            monthBorderColor="#ffffff"
            dayBorderWidth={2}
            dayBorderColor="#ffffff"
            legends={[
                {
                    anchor: 'bottom-right',
                    direction: 'row',
                    translateY: 36,
                    itemCount: 4,
                    itemWidth: 42,
                    itemHeight: 36,
                    itemsSpacing: 14,
                    itemDirection: 'right-to-left'
                }
            ]}
        />
    )
  }
}

// =============================================================================
// The dataviz container

// This is where we will handle the logic of gathering the data, and of
// rendering the dataviz inside the proper container

const ExampleCalendar = () => {
  // For this dataviz I am not using this, BUT
  // because some dataviz might use it, I left this here for documentation
  // The UserContext contains the user object IF the user is logged in.
  // This is useful if we want to get some user specific viz such as an Ego Network
  const userContext = useContext(UserContext);
  // Look in the console so that you can see how the use object looks like
  console.log(userContext.user);

  // Here we define the state variables that are needed for this to function
  // State variable in React is a key concept, they are monitored by the React
  // engine. Therefore, if we change them, they can trigger a cascade of re-render
  // to learn more about state: https://reactjs.org/docs/state-and-lifecycle.html

  // First the data, this will holds ... our data !
  const [data, setData] = useState([])
  // Then some usefull state such as loading, and refresh
  const [loading, setLoading] = useState(true)
  // If you toggle refresh using setRefresh(!refresh)
  // you will retrigger the API call
  // Can be usefull ... ?
  const [refresh, setRefresh] = useState(false)
  // useEffect will be run when the component is loaded
  // So "On Load -> Do what is inside me"
  useEffect(() => {
    // Here we check that our data is not loaded already
    // no need to make the backend suffer from many calls
    if (data.length === 0) {
      // Here we define our API call
      // THIS IS IMPORTANT
      // The url: "slack/get_reactions_count" corresponds to:
      // http://the.backend.url/api/slack/get_reactions_count
      // and it will also send the correct credentials for you
      // basically, you only have to ask for the correct API endpoint and
      // you will receive the data back
      Api.get("slack/get_reactions_count")
      .then(res => {
        // we receive the data and we do something with it
        console.log(res.data)

        // here we take the the "reactions" array and save it into our State: data
        setData(res.data.reactions);
        // After we set data, react will then -> Re-Render the dataviz
        // because the dataviz DEPENDS on data, and data is a state

        // Then we set the loading state to false
        setLoading(false);
        // This will remove the loading icon
      })
    }
    // Here we say that useEffect is DEPENDENT upon "refresh"
    // So if we toggle refresh we can redownload the data
  }, [data])


  // This is so that the refresh removes the data
  useEffect(() => {
    setData([])
  }, [refresh])

  // ===========================================================================
  // Container definition

  // Here I created a "special case, where you can test the full car or the
  // split card

  // /!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\
  // IN REALITY you only chose ONE of the two !!! like so:

  // return (
  //   <DatavizCardFull
  //     title="THE DATAVIZ TITLE"
  //     dataviz={<Calendar data={data}/>}
  //     unique_id="AUNIQUEID"
  //     loading={loading}
  //     height={500}
  //   >
  //     The text that describes the dataviz
  //   </DatavizCardFull>
  // )

  // Change this to false to see how it looks like in a split vs full
  const full=true;

  if (full) {
    return (
      <DatavizCardFull
        title="Example dataviz" // Set a fancy title
        dataviz={<Calendar data={data}/>} // Here, you are sending the Calendar we have created above !!
        unique_id="myExampleViz" // THIS NEEDS TO BE UNIQUE !!! If it is not, then the download button will fail
        loading={loading}
        height={500}
      >
        This is an open HTML area to write some cool text!
        you can <b>Bold</b> or <i>italicize</i> or anything else that you <u>fancy</u>
        or add a
        <ul>
          <li>list</li>
          <li>of</li>
          <li>items</li>
        </ul>
      </DatavizCardFull>
    )
    // Same as above just that we use the DatavizCardSplit Instead
  } else {
    return (
      <DatavizCardSplit
        title="Example dataviz"
        dataviz={<Calendar data={data}/>}
        unique_id="myExampleViz"
        loading={loading}
        height={500}
      >
        This graph shows you who mentions who in your slack workspace. Each user is connected to the other users in the workspace by a line. The line thickness represents the number of mentions from one person to the other.
      </DatavizCardSplit>
    )
  }

}

// =============================================================================
// ADD IT TO THE INSIGHTS
// go to the /pages/Insights.js

export default ExampleCalendar;
