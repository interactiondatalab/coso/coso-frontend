import React, { useState, useEffect, useContext } from 'react';
import { Card, Icon, Grid, Header, Button } from 'semantic-ui-react';
import "./Header.css";
import Avatar from 'react-avatar';
import IgemUserIDSelect from "../utils/IgemUserIDSelect";
import Api from "../Api"
import { UserContext } from "../utils/UserContext"

const UserCard = ({user, setRefresh}) => {
  const [username, setUsername] = useState("")
  const [editUsername, setEditUsername] = useState(false)
  const userContext = useContext(UserContext);
  const [hideUsernameInput, setHideUsernameInput] = useState(true);

  useEffect(() => {
    if (user.custom) {
      setHideUsernameInput(false);
    }
    setUsername(user.username)
  }, [user])

  const saveUserId = (value) => {
    console.log(value)
    console.log(user)
    const igem_user = {
      user_id: user.user.id,
    }
    Api.patch(`igem_users/${value}`, {igem_user})
    .then(res => {
      Api.delete(`igem_users/${user.id}`)
      .then(res => {
        setHideUsernameInput(true);
        setRefresh(true);
      })
      user = res.data
    })
    .catch(error => {
      console.log(error)
    })
  }

  console.log(user)

 return (
  <Card>
    <Card.Content>
      <Avatar className="ui mini right floated image avatar" name={user.full_name} alt={user.full_name} size="32px" round="2" email={user.user == null ? "" : user.user.email} />
      <Card.Header>{user.full_name}</Card.Header>
      <Card.Meta>
        {
          hideUsernameInput ?
            <span>UserName: {username}</span>
          :
            <>
            <span className="ui red basic label" style={{border:"0"}}>Missing iGEM username</span>
            {editUsername ?
              <IgemUserIDSelect
                teamId={userContext.user.team.id}
                setIgemUserId={saveUserId}
                save={true}
              />
              :
              <Button compact basic size="mini" onClick={() => {setEditUsername(true)}}>+Add iGEM username</Button>
            }
            </>
        }
      </Card.Meta>
    </Card.Content>
    <Card.Content extra style={{paddingBottom:"20px"}}>
      <Header as="h4" style={{paddingBottom:"6px"}}>Activity:</Header>
      <Card.Description>
        <Grid columns={3}>
          <Grid.Row  style={{paddingTop:"0.2rem", paddingBottom:"0.4rem"}}>
            <Grid.Column width={1}>
              <Icon  name='tasks' />
            </Grid.Column>
            <Grid.Column width={6}>
              <span>{user.user == null ? "0" : user.user.log_count} Tasks</span>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Card.Description>
      <Header as="h4" style={{paddingBottom:"6px"}}>Registered on:</Header>
      <Card.Description>
        <Grid columns={3}>
          <Grid.Row  style={{paddingTop:"0.2rem", paddingBottom:"0.4rem"}}>
            <Grid.Column width={1}>
              <Icon name="computer" disabled={user.user == null} />
            </Grid.Column>
            <Grid.Column width={4}>
              <span disabled={user.user == null}>Website</span>
            </Grid.Column>
            <Grid.Column width={1}>
              {user.user == null ? <Icon color="red" name="times circle outline" /> : <Icon color="green" name="check circle outline" />}
            </Grid.Column>
          </Grid.Row>
          <Grid.Row  style={{paddingTop:"0.2rem", paddingBottom:"0.4rem"}}>
            <Grid.Column width={1}>
              <Icon name="mobile alternate" disabled={user.user == null ? true : !user.user.phones.length > 0}/>
            </Grid.Column>
            <Grid.Column width={4}>
              <span disabled={user.user == null ? true : !user.user.phones.length > 0}>App</span>
            </Grid.Column>
            <Grid.Column width={1}>
              {user.user == null ? true : !user.user.phones.length > 0 ? <Icon color="red" name="times circle outline" /> : <Icon color="green" name="check circle outline" />}
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Card.Description>
    </Card.Content>
  </Card>
)};

export default UserCard

export { UserCard };
