import React, {useEffect, useState, useContext} from 'react';
import { Card,
         Icon,
         Grid,
         Segment,
         Button,
         Input,
         Label } from 'semantic-ui-react';
import "./Header.css";
import Api from "../Api";
import { UserContext } from "../utils/UserContext";
import UserCard from "../components/UserCard";
import Users from "./UserItems";
import AddIgemUserModal from "../utils/AddIgemUserModal"

const TeamDisplay = () => {
  const user = useContext(UserContext);
  const [loading, setLoading] = useState(true);
  const [refresh, setRefresh] = useState(false);
  const [team, setTeam] = useState([]);
  const [filteredtTeam, setFilteredTeam] = useState([]);
  const [igemFilter, setIgemFilter] = useState(false);
  const [teamDisplay, setTeamDisplay] = useState("cards");
  const [missingIgemUser, setMissingIgemUser] = useState([]);

  useEffect(() => {
    if (user.user.team) {
      Api.get("/teams/" + user.user.team.id + "/igem_users")
      .then(res => {
        setTeam(res.data);
        setFilteredTeam(res.data);
        setLoading(false);
      })
    }
  }, [user, refresh]);

  useEffect(() => {
    if (refresh) {
      setRefresh(false)
    }
  }, [refresh])

  useEffect(() => {
    filterTeam("")
  }, [igemFilter])

  useEffect(() => {
    setMissingIgemUser(team.filter(user => (user.custom)))
  }, [team])

  console.log(team);

  const filterTeam = ( value ) => {
    const result = team.filter(user => {
      if (igemFilter) {
        return user.custom
      } else {
        if (user.full_name && user.full_name.toLowerCase().includes(value.toLowerCase())) {
          return true
        } else if (user.username && user.username.toLowerCase().includes(value.toLowerCase())) {
          return true
        } else {
          return false
        }
      }
    });
    setFilteredTeam(result);
  };

  return (
    <>
      <Segment basic loading={loading}>
      <Grid stackable columns={3}>
        <Grid.Column width={10}>
          <Label.Group>
            <Button  size="small" onClick={()=>{setIgemFilter(false)}}>
              <Icon name='code branch' /> {team.length} Team members
            </Button>
            { missingIgemUser.length > 0 &&
              <Button size="small" color="red" basic={!igemFilter} onClick={() => setIgemFilter(!igemFilter)}>
                <Icon name='warning circle' /> {missingIgemUser.length} members not linked to iGEM
              </Button>
            }
            <AddIgemUserModal refreshTeam={setRefresh} />
          </Label.Group>
        </Grid.Column>
        <Grid.Column textAlign="right"  width={4}>
          <Input icon='search' onChange={(e, { value }) => {filterTeam(value)}} placeholder='Search...' />
        </Grid.Column>
        <Grid.Column textAlign="right" width={2}>
          <Button.Group>
            <Button active={teamDisplay === "cards"} icon onClick={()=>{setTeamDisplay("cards")}}>
              <Icon name='th' />
            </Button>
            <Button active={teamDisplay === "lists"} icon onClick={()=>{setTeamDisplay("lists")}}>
              <Icon name='th list' />
            </Button>
          </Button.Group>
        </Grid.Column>
      </Grid>
    </Segment>
  {teamDisplay === "cards" ?
    <Segment basic>
      <Card.Group centered>
        {filteredtTeam.map(function(user, i){
          return <UserCard user={user} key={i} setRefresh={setRefresh} />;
        })}
      </Card.Group>
    </Segment>
  :
    <Users team={filteredtTeam} setRefresh={setRefresh}/>
  }
  </>
  )
}

export default TeamDisplay
