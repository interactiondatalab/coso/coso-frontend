import React, {useEffect, useState } from 'react';
import { Segment, Button, Header, Item, Modal, Image, Icon } from 'semantic-ui-react';
import Api from "../Api"
import SendNewsModal from "../utils/SendNewsModal"
import { Link, useParams } from "react-router-dom";


function News( props ) {
  const [news, setNews] = useState({})
  let { newsId } = useParams();

  useEffect(() => {
    if (props.webview) {
      Api.get('/news/'+newsId)
      .then(res => {
        setNews(res.data);
      })
      .catch(error => {
        console.log(error);
      })
    } else {
      setNews(props.news)
    }
  }, [])

  return (
    <>
    {props.webview &&
      <Segment basic>
      <Button
        icon
        onClick={() => {window.ReactNativeWebView.postMessage("back")}}
        style={{
          backgroundColor: "#e4f5f6",
          color:"#3a8c90"}}
      >
        <Icon size="big" name="angle left"/>
      </Button>
      </Segment>
    }
    <Segment basic textAlign='center'>
      <Header as="h1">
        {news.title}
      </Header>
      <Image centered src={news.image} />
      <Segment basic>
        {news.content}
      </Segment>
        {
          props.webview ?
          <Button primary onClick={() => {window.ReactNativeWebView.postMessage(JSON.stringify({openURL: news.action_url}))}}>{news.action_text}</Button>
          :
          <Button primary as="a" href={news.action_url}>{news.action_text}</Button>
        }
    </Segment>
  </>
  )
}

function NewsModal( props ) {
  const [open, setOpen] = useState(false);
  const [isAdmin, setIsAdmin] = useState(false);

  useEffect(() => {
    if (open) {
      Api.get('/news/'+props.news.id)
      .then(res => {
        props.setNews(res.data);
      })
      .catch(error => {
        console.log(error);
      })
    }
  }, [open, props])

  useEffect(() => {
    Api.get('/users/is_admin')
    .then(res => {
      setIsAdmin(true);
    })
    .catch(error => {
      setIsAdmin(false);
    })
  });

  function onDelete() {
    Api.delete('/news/'+props.news.id)
    .then(res => {
      props.setRefresh(true);
      setOpen(false)
    })
    .catch(error => {
      console.log(error)
      setOpen(false)
    })
  }

  return (
    <Modal
      closeIcon
      dimmer="blurring"
      size="tiny"
      open={open}
      trigger={props.children}
      onClose={() => setOpen(false)}
      onOpen={() => setOpen(true)}
    >
    <Modal.Content>
      <News news={props.news} />
    </Modal.Content>
    <Modal.Actions>
      {isAdmin ? <Button color="red" onClick={onDelete}>Delete</Button> : null}
      <Button color="grey" onClick={() => (setOpen(false))}>Close</Button>
    </Modal.Actions>
    </Modal>
  )
}

function NewsInline( props ) {
  const [news, setNews] = useState(props.news);

  return (
    <NewsModal news={news} setNews={setNews} setRefresh={props.setRefresh}>
      <Item>
        <Item.Image size='tiny' src={news.image} />
        <Item.Content textAlign="left" verticalAlign='middle'>
          <Item.Header as={Header} disabled={news.unread}>{news.title}</Item.Header>
        </Item.Content>
      </Item>
    </NewsModal>
  )
}

function NewsGroup( props ) {
  const [refresh, setRefresh] = useState(true);
  const [news, setNews] = useState([]);
  const [loading, setLoading] = useState(true);
  const [isAdmin, setIsAdmin] = useState(false);

  useEffect(() => {
    if (refresh) {
      setNews([])
      Api.get('/news')
      .then(res => {
        const tmp = [];
        res.data.forEach((item, i) => {
          tmp.push(<NewsInline news={item} key={i} setRefresh={setRefresh}/>)
        });
        setNews(tmp);
        setLoading(false);
      })
      .catch(error => {
        console.log(error);
        setLoading(false);
      })
      setRefresh(false);
    }
  }, [refresh])

  useEffect(() => {
    Api.get('/users/is_admin')
    .then(res => {
      setIsAdmin(true);
    })
    .catch(error => {
      setIsAdmin(false);
    })
  });

  return (
    <Segment basic loading={loading}>
      <Item.Group divided>
        {news}
      </Item.Group>
      {isAdmin && <SendNewsModal setRefresh={setRefresh} />}
    </Segment>
  )
}

export { News, NewsInline, NewsGroup, NewsModal };
