import React, { Component, useState, useEffect, useContext } from 'react';
import { clone } from "../utils/Functions";
import { Segment,
         Button,
         Header,
         Form,
         Item,
         Icon,
         Tab,
         Label,
         Radio,
         Message,
         Grid,
         Container,
         Divider,
         TextArea,
         Card } from 'semantic-ui-react';
import { UserContext } from "../utils/UserContext";
import { Slider } from "react-semantic-ui-range";
import { Link, useParams } from "react-router-dom";
import Api from "../Api";
import SendSurveyModal from "../utils/SendSurveyModal";
import { YearInput } from 'semantic-ui-calendar-react';

const InputSelect = ( { field, value, setValue } ) => {
  const options = field.content.options.map((item, i) => (
    {
      key: i,
      value: item.value,
      text: item.label
    }
  ))

  return (
    <Form.Field>
      <Form.Dropdown
        placeholder={field.content.placeholder}
        options={options}
        onChange={(e, {value}) => {setValue(value)}}
        value={value}
      />
    </Form.Field>

  )
}

const InputField = ( { field, value, setValue } ) => {

  return (
    <Form.Field>
      <Form.Input
        placeholder={field.content.placeholder}
        value={value}
        onChange={(e, {value}) => setValue(value)}
      />
    </Form.Field>
  )
}

const InputTextfield = ( { field, value, setValue } ) => {
  return (
    <Form.Field>
      <TextArea
        fluid
        rows={2}
        value={value}
        placeholder={field.content.placeholder}
        onChange={(e, {value}) => setValue(value)}
      />
    </Form.Field>
  )
}

const InputSlider = ( { field, value, setValue } ) => {
  const settings = {
      start: field.content.min,
      min: field.content.min,
      max: field.content.max,
      step: field.content.step,
      onChange: value => {
        setValue(value);
      }
    };

  return (
    <Form.Field>
      <Grid columns={3}>
        <Grid.Column textAlign="right">
          <Label basic size="large" content={field.content.labels.min} />
        </Grid.Column>
        <Grid.Column verticalAlign="middle" textAlign="center">
          <Slider color="teal" discrete value={value} settings={settings} style={{thumb: {background: "rgb(0, 181, 173)"}}}/>
        </Grid.Column>
        <Grid.Column textAlign="left">
          <Label basic size="large" content={field.content.labels.max} />
        </Grid.Column>
      </Grid>
    </Form.Field>
  )
}

const InputRadioButton = ( { field, value, setValue } ) => {
  return (
    <Form.Field>
      <Grid columns={3}>
        <Grid.Column textAlign="right">
          <Label basic size="large" content={field.content.options[0].label} color={value ? "grey" : "teal"} onClick={() => setValue(false)}/>
        </Grid.Column>
        <Grid.Column verticalAlign="middle" textAlign="center">
          <Radio
            toggle
            checked={value}
            color="teal"
            onChange={() => setValue(!value)}
          />
        </Grid.Column>
        <Grid.Column textAlign="left">
          <Label basic size="large" content={field.content.options[1].label}  color={value ? "teal" : "grey"} onClick={() => setValue(true)}/>
        </Grid.Column>
      </Grid>
    </Form.Field>
  )
}

const RadioMatrixMobile = ({row_index, column, field, anwser, setAnwser}) => (
  <Grid.Row style={{padding:"5px"}} columns={2} only='mobile' centered>
    <Grid.Column textAlign="right" verticalAlign="middle">
      <Form.Radio
        name={field.name + "_mobile_" + row_index}
        value={column.value}
        checked={anwser[row_index] === column.value}
        onChange={(e, { value }) => setAnwser(row_index, value)}
      />
    </Grid.Column>
    <Grid.Column textAlign="left">
      {column.sublabel && <Label basic color="teal">{column.sublabel}</Label>}
    </Grid.Column>
  </Grid.Row>
)

const RadioMatrixDesktop = ({row_index, column, field, anwser, setAnwser}) =>  (
  <Grid.Column only='tablet computer' textAlign="center" verticalAlign="bottom">
      {column.sublabel &&
        <Label color='teal' basic>
          {column.sublabel}
        </Label>
      }
      <Form.Radio
        name={field.name + "_" + row_index}
        value={column.value}
        label={column.label === "" ? null : column.label}
        checked={anwser[row_index] === column.value}
        onChange={(e, { value }) => setAnwser(row_index, value)}
      />
  </Grid.Column>
)

const InputRadioMatrix = ( { field, value, setValue } ) => {

  const setAnwser = (row, val) => {
    const tmp = value;
    tmp[row] = val;
    setValue(tmp);
  }

    const rows = field.content.options.rows.map((row, i) => {
      const columns = field.content.options.columns.map((column, j) => {
        return (
          <>
          <RadioMatrixMobile row_index={i} column={column} field={field} anwser={value} setAnwser={setAnwser}/>
          <RadioMatrixDesktop row_index={i} column={column} field={field} anwser={value} setAnwser={setAnwser}/>
          </>
        )
      });

      return (
        <Form.Field key={i}>
          <Segment basic>
            {row.label}
          </Segment>
          <Grid columns={field.content.options.columns.length}>
            {columns}
          </Grid>
        </Form.Field>
      )
    })


  return (
    rows
  )
}

const InputRadioList = ( { field, value, setValue } ) => {
  const radios = field.content.options.map((item, i) => {
    return (
      <Form.Field key={i}>
        <Form.Radio
          label={item.label}
          name={field.name}
          value={item.value}
          checked={value === item.value}
          onChange={(e, { value }) => setValue(value)}
        />
      </Form.Field>
    )
  });

  return (
    <Form.Field>
      {radios}
    </Form.Field>
  )
}

const InputCheckbox = ( { field, value, setValue } ) => {
  const anwsers = value;

  const checkboxes = field.content.options.map((item, i) => {
    return (
      <Form.Checkbox
        label={item.label}
        name={field.name}
        checked={value[i]}
        onChange={() => {anwsers[i] = !value[i]; setValue(anwsers)}}
      />
    )
  });

  return (
    <Form.Field>
      {checkboxes}
    </Form.Field>
  )
}

const ListNames = ( { field, value, setValue } ) => {

  const userContext = useContext(UserContext);
  const [team, setTeam] = useState([]);
  const [loading, setLoading] = useState([]);
  const [names, setNames] = useState([]);
  const [refresh, setRefresh] = useState(false);

  useEffect(() => {
    if (userContext.user.team) {
      Api.get("/teams/" + userContext.user.team.id + "/igem_users")
      .then(res => {
        setTeam(res.data);
        setLoading(false);
      })
      .catch(error => {
        console.log(error);
      })
    }
  }, [userContext]);


  const toggleAnswer = (username) => {
    const tmp = value;
    if (tmp.includes(username)) {
      tmp.splice(value.indexOf(username, 1));
    } else {
      tmp.push(username);
    }
    setRefresh(!refresh)
    setValue(tmp);
  }

  useEffect(() => {
    const tmp = team.map((item, i) => {
      return (
        <Card key={i}>
          <Button
            content={item.full_name ? item.full_name : item.username}
            toggle
            active={value.includes(item.username)}
            onClick={() => {toggleAnswer(item.username)}}
          />
        </Card>
      )
    });
    setNames(tmp);
  }, [team, refresh])


  return (
    <Segment basic loading={loading}>
      <Form.Field>
        <Card.Group>
          {names}
        </Card.Group>
      </Form.Field>
    </Segment>
  )
}

const InputDate = ( { field, value, setValue } ) => {
  const handleChange = (event, {name, value}) => {
    setValue(value)
  }

  return (
    <Form.Field>
      <YearInput
        name="date"
        placeholder="Date"
        value={value}
        iconPosition="left"
        dateFormat="YYYY"
        onChange={handleChange}
      />
    </Form.Field>

  )
}

const SurveyField = ( { field, value, setValue } )  => {
  return (
    <Form>
      <Form.Field>
        {field.content.text}
      </Form.Field>
      {field.category === "inputRadioMatrix" && <InputRadioMatrix field={field} value={value} setValue={setValue} />}
      {field.category === "inputRadioList" && <InputRadioList field={field} value={value} setValue={setValue} />}
      {field.category === "inputCheckbox" && <InputCheckbox field={field} value={value} setValue={setValue} />}
      {field.category === "inputRadioButton" && <InputRadioButton field={field} value={value} setValue={setValue} />}
      {field.category === "inputSlider" && <InputSlider field={field} value={value} setValue={setValue} />}
      {field.category === "inputTextfield" && <InputTextfield field={field} value={value} setValue={setValue} />}
      {field.category === "inputField" && <InputField field={field} value={value} setValue={setValue} />}
      {field.category === "inputSelect" && <InputSelect field={field} value={value} setValue={setValue} />}
      {field.category === "listNames" && <ListNames field={field} value={value} setValue={setValue} />}
      {field.category === "inputDate" && <InputDate field={field} value={value} setValue={setValue} />}
    </Form>
  )
}


const Survey = ( props ) => {
  let { surveyId } = useParams();
  if (!surveyId && props.id) {
    surveyId = props.id;
  }

  const [activeIndex, setActiveIndex] = useState(0);
  const [maxIndex, setMaxIndex] = useState(0);
  const [loading, setLoading] = useState(false);
  const [panes, setPanes] = useState([]);
  const [survey, setSurvey] = useState({});
  const [anwsers, setAnwsers] = useState([]);
  const [message, setMessage] = useState(null);
  const [isValid, setIsValid] = useState(false);

  useEffect(() => {
    console.log("received props")
    console.log({props})
    if (props.survey) {
      setActiveIndex(0);
      setSurvey(props.survey)
      setMaxIndex(props.survey.survey_fields.length);
    }
  }, [props])

  useEffect(() => {
    const index = activeIndex;
    if (activeIndex > 0) {
      if (survey.survey_fields[activeIndex-1].required) {
        if (survey.survey_fields[activeIndex-1].category === "inputRadioMatrix") {
          var test = false
          anwsers[activeIndex-1].content.forEach((el) => {
            if (!el) {
              test = true;
            }
          })
          if (test) {
            setIsValid(false)
          } else {
            setIsValid(true)
          }
        }
        else if (survey.survey_fields[activeIndex-1].category === "inputRadioList" & anwsers[activeIndex-1].content === "") {setIsValid(false)}
        // else if (survey.survey_fields[activeIndex-1].category === "inputCheckbox" & anwsers[activeIndex-1].content === []) {setIsValid(false)}
        // else if (survey.survey_fields[activeIndex-1].category === "inputRadioButton" & anwsers[activeIndex-1].content === []) {setIsValid(false)}
        else if (survey.survey_fields[activeIndex-1].category === "inputSlider" & anwsers[activeIndex-1].content === -1) {setIsValid(false)}
        else if (survey.survey_fields[activeIndex-1].category === "inputTextfield" & anwsers[activeIndex-1].content === "") {setIsValid(false)}
        else if (survey.survey_fields[activeIndex-1].category === "inputField" & anwsers[activeIndex-1].content === "") {setIsValid(false)}
        else if (survey.survey_fields[activeIndex-1].category === "inputSelect" & anwsers[activeIndex-1].content === "") {setIsValid(false)}
        else if (survey.survey_fields[activeIndex-1].category === "listNames" & anwsers[activeIndex-1].content.length === 0) {setIsValid(false)}
        else if (survey.survey_fields[activeIndex-1].category === "inputDate" & anwsers[activeIndex-1].content === "") {setIsValid(false)}
        else {
          setIsValid(true)
        }
      } else {
        setIsValid(true)
      }
    } else {
      setIsValid(true)
    }
  }, [activeIndex, anwsers]);

  const handleNextTab = () => {
    console.log(anwsers)
    if (activeIndex < maxIndex & isValid) {
      setActiveIndex(activeIndex + 1)
    }
  }

  const handlePrevTab = () => {
    if (activeIndex > 0) {
      setActiveIndex(activeIndex - 1)
    }
  }

  const handleSubmit = () => {
    setLoading(true)
    const data = anwsers;
    Api.post(`/surveys/${surveyId}/answer`, {data})
    .then(res => {
      setMessage({success: true, title:"Congatulation", content:"Your anwsers have been saved, thank you for your participation in this survey!"})
    })
    .catch(error => {
      setMessage({success: false, title:"Something went wrong", content: error[0]})
      console.log(error);
    })
    .then(() => {
      setLoading(false);
    })
  }

  function prepareAnwsers() {
    if (survey.survey_fields) {
      const tmp = survey.survey_fields.map((item, i) => {
        const base = {
          survey_field_id: item.id,
          content: null
        }
        if (item.category === "inputRadioMatrix") {base.content = item.content.options.rows.map((item, i) => null)}
        if (item.category === "inputRadioList") {base.content = ""}
        if (item.category === "inputCheckbox") {base.content = item.content.options.map((item, i) => false)}
        if (item.category === "inputRadioButton") {base.content = false}
        if (item.category === "inputSlider") {base.content = 0}
        if (item.category === "inputTextfield") {base.content = ""}
        if (item.category === "inputField") {base.content = ""}
        if (item.category === "inputSelect") {base.content = ""}
        if (item.category === "listNames") {base.content = []}
        if (item.category === "inputDate") {base.content = ""}
        return (
          base
        )
      })
      setAnwsers(tmp)
    }
  }

  const setValue = (i, value) => {
    const tmp = anwsers.map((item, index) => {
      if (index == i) {
        item.content = value
      }
      return (
        item
      )
    })
    console.log(tmp);
    setAnwsers(tmp);
  }

  function makePanes() {
    if (survey.survey_fields) {
      const tmp = survey.survey_fields.map((item, i) => {
        return (
          {
          // menuItem: `Question ${i+1}`,
          render: () => (
            <Tab.Pane attached={false} style={{backgroundColor: props.webview ? "#e4f5f6" : "#fff"}}>
              <Header content={item.name} />
              <SurveyField
                field={item}
                value={anwsers[i].content}
                setValue={(value) => setValue(i, value)}
              />
            </Tab.Pane>
            )
          }
        )
      })
      tmp.unshift({
        // menuItem: "Introduction",
        render: () => (
          <Tab.Pane attached={false} style={{backgroundColor: props.webview ? "#e4f5f6" : "#fff"}}>
            <Header content={survey.name} />
            {survey.description}
          </Tab.Pane>
        )
      })
      setPanes(tmp)
    }
  }

  useEffect(() => {
    if (props.webview) {
      var style = document.createElement('style');
      style.type = 'text/css';
      style.innerHTML = '.ui input { background: #e4f5f6 !important; } .ui.basic.label  { background: #e4f5f6 !important;} .ui.form textarea { background: #e4f5f6 !important;} .ui.basic.grey.label { background: #e4f5f6 !important;} .ui.basic.teal.label { background: #e4f5f6 !important;}';
      document.getElementsByTagName('head')[0].appendChild(style);
    }
  }, [])

  useEffect(() => {
    prepareAnwsers()
  }, [survey, props])

  useEffect(() => {
    makePanes()
  }, [anwsers, props])

  useEffect(() => {
    if (Object.keys(survey).length === 0) {
      Api.get(`/surveys/${surveyId}`)
      .then(res => {
        setSurvey(res.data);
        setMaxIndex(res.data.survey_fields.length);
      })
      .catch(error => {
        console.log(error)
      })
      .then(
        setLoading(false)
      )
    }
  }, [survey])

  if (props.webview) {
    return (
      <Container style={{height:"100vh"}}>
      <Segment basic>
      <Button
        icon
        onClick={() => {window.ReactNativeWebView.postMessage("back")}}
        style={{
          backgroundColor: "#e4f5f6",
          color:"#3a8c90"}}
      >
        <Icon size="big" name="angle left"/>
      </Button>
      </Segment>
      <Tab
      menu={{ secondary: true, style: {display: "None"} }}
      panes={panes}
      activeIndex={activeIndex}
      style={{backgroundColor: "#e4f5f6"}}
      />
      <Divider />
      {message &&
        <Message attached='bottom' success={message.success} error={!message.error}>
        <Message.Header>{message.title}</Message.Header>
        <p>
        {message.content}
        </p>
        </Message>
      }
      <Segment basic>
      {activeIndex > 0 || (message && !message.success) ? <Button floated='left' color="grey" onClick={handlePrevTab}>Previous</Button> : null}
      {activeIndex < maxIndex && <Button floated='right' color={isValid ? "teal" : "grey"} onClick={handleNextTab}>Next</Button>}
      {maxIndex > 0 && activeIndex === maxIndex ? message && message.success ? <Button floated='right' color="teal" onClick={() => {window.ReactNativeWebView.postMessage("close")}}>Close</Button> : <Button floated='right' color={isValid ? "primary" : "grey"} onClick={handleSubmit}>Save</Button> : null}
      </Segment>
      </Container>
    )
  } else {
    return (
      <Segment basic>
        <Segment vertical loading={loading}>
        <Tab
            menu={{ secondary: true, style: {display: "None"} }}
            panes={panes}
            activeIndex={activeIndex}
        />
        </Segment>
        {message &&
          <Message attached='bottom' success={message.success} error={!message.error}>
            <Message.Header>{message.title}</Message.Header>
            <p>
              {message.content}
            </p>
          </Message>
        }
        <Segment basic>
        {activeIndex > 0 || (message && !message.success) ? <Button floated='left' color="grey" onClick={handlePrevTab}>Previous</Button> : null}
        {activeIndex < maxIndex && <Button floated='right' color={isValid ? "teal" : "grey"} onClick={handleNextTab}>Next</Button>}
        {maxIndex > 0 && activeIndex === maxIndex ? message && message.success ? <Button floated='right' color="teal" as={Link} to="/surveys">Close</Button> : <Button floated='right' color={isValid ? "primary" : "grey"} onClick={handleSubmit}>Save</Button> : null}
        </Segment>
      </Segment>
    )
  }
}

const SurveyInline = ( {survey, key} ) => {
  const [done, setDone] = useState(false);

  useEffect(() => {
    Api.get(`surveys/${survey.id}/is_completed`)
    .then(res => {
      if (res.data > 0) {
        setDone(true);
      }
    })
    .catch(error => {
      console.log(error);
    })
  }, [survey])
  // <Item as="Link" to={`/surveys/${survey.id}`}>

  return (
    <Item as={Link} to={done ? "/surveys" : `/surveys/${survey.id}`}>
      <Icon color="teal" size='large' name={done ? "clipboard check" : "clipboard list"} />
      <Item.Content textAlign="left" verticalAlign='middle'>
        <Item.Header as={Header} disabled={done}>{survey.name}</Item.Header>
        <Item.Meta>{survey.minutes}</Item.Meta>
        <Item.Description>{survey.description}</Item.Description>
      </Item.Content>
    </Item>
  )
}

function Surveys( props ) {
  const [refresh, setRefresh] = useState(false);
  const [surveys, setSurveys] = useState([]);
  const [loading, setLoading] = useState(true);
  const [isAdmin, setIsAdmin] = useState(false);

  useEffect(() => {
    if (surveys.length !== []) {
      Api.get('/surveys')
      .then(res => {
        const tmp = [];
        res.data.forEach((item, i) => {
          tmp.push(<SurveyInline survey={item} key={i}/>)
        });
        tmp.reverse();
        setSurveys(tmp);
        setLoading(false);
      })
      .catch(error => {
        console.log(error);
        setLoading(false);
      })
      setRefresh(false);
    }
  }, [refresh])

  useEffect(() => {
    setSurveys([]);
  }, [refresh])

  useEffect(() => {
    Api.get('/users/is_admin')
    .then(res => {
      setIsAdmin(true);
    })
    .catch(error => {
      setIsAdmin(false);
    })
  });

  return (
    <Segment basic loading={loading}>
      <Item.Group divided>
        {surveys}
      </Item.Group>
      {isAdmin && <SendSurveyModal setRefresh={setRefresh} />}
    </Segment>
  )
}

export { Surveys, Survey, SurveyField, InputRadioList, SurveyInline }
