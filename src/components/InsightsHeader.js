import React from 'react';
import { Container, Segment, Header } from 'semantic-ui-react';

function InsightsHeader() {
  return (
    <Container>
      <Segment basic>
        <Header as="h1">
          Your Team Insights
        </Header>
          Based on the data collected from CoSo app and other sources, you can find here your Team Insights. Explore, Review and Download dataviz to highlight how your team collaborate
      </Segment>
    </Container>
  )
}

export { InsightsHeader };
