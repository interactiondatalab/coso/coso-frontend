import React from 'react';
import { Segment, Button } from 'semantic-ui-react';
import "./Header.css";

function Header() {
  return (
    <Segment basic textAlign='center' fluid inverted className="home-header">
      <Segment vertical inverted>
        <h1>Welcome to <b>iGEM TIES</b></h1>
      </Segment>
      <Segment vertical inverted>
        <h2>Mapping iGEM team interactions</h2>
      </Segment>
      <Segment vertical inverted>
        <Button inverted>Learn More</Button>
      </Segment>
    </Segment>
  )
}

export { Header };
