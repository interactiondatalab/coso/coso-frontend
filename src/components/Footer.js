import React from 'react';
import { Segment, Grid, Icon, Header } from 'semantic-ui-react';
import "./Footer.css";

function Footer() {
  return (
    <Segment textAlign='center' fluid inverted>
      <Segment vertical inverted>
        <Header as="h1">Follow us on social media!</Header>
      </Segment>
      <Segment vertical inverted>
        <Grid columns={3} stackable textAlign='center'>
          <Grid.Row verticalAlign='middle'>
            <Grid.Column>
              <a href="https://www.facebook.com/iGEMTies" rel="noreferrer" target="_blank">
              <Header inverted icon>
                <Icon inverted name="facebook official" size="massive"/>
                Facebook
              </Header>
              </a>
            </Grid.Column>
            <Grid.Column>
              <a href="https://www.instagram.com/igem.ties/" rel="noreferrer" target="_blank">
              <Header inverted icon>
                <Icon inverted name="instagram" size="massive"/>
                Instagram
              </Header>
              </a>
            </Grid.Column>
            <Grid.Column>
              <a href="https://twitter.com/IgemTies" rel="noreferrer" target="_blank">
              <Header inverted icon>
                <Icon name="twitter" size="massive"/>
                Twitter
              </Header>
              </a>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Segment>
      <Segment vertical inverted>
        <Header as="h2">Contact us at <a href="igem-ties@cri-paris.org">igem-ties@cri-paris.org</a></Header>
      </Segment>
    </Segment>
  )
}

export { Footer };
