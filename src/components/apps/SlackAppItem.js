import { useState, useEffect, useContext } from 'react';
import { UserContext } from "../../utils/UserContext"
import { Button } from 'semantic-ui-react';
import Api from '../../Api';
import { AppItem } from "../AppItems"

function SlackAppItem () {
  const [exists, setExists] = useState(false);
  const [link, setLink] = useState("");
  const [loading, setLoading] = useState(true);
  const userContext = useContext(UserContext);

  useEffect(() => {
    if (!exists) {
      if (userContext.user.username) {
        setLoading(true);
        Api.get('/slack/exists')
        .then(res => {
          setExists(true);
          setLoading(false);
        })
        .catch(error => {
          setLoading(false);
          console.log(error);
        })
      }
    }
  }, [exists, userContext])

  useEffect(() => {
    if (link === "") {
      if (userContext.user.username) {
        setLoading(true);
        Api.get('/auth/slack')
        .then(res => {
          setLink(res.data.url);
          setLoading(false);
        })
        .catch(error => {
          console.log(error);
          setLoading(false);
        })
      }
    }
  }, [link, userContext])

  return (
    <AppItem
      name="Slack"
      icon="https://image.flaticon.com/icons/png/512/2111/2111615.png"
      description="connect your slack here"
      cta={<Button loading={loading} as='a' href={link}>{!exists ? "Connect" : "Disconnect"}</Button>}
      status={exists}
    />
  )
}

export default SlackAppItem;
