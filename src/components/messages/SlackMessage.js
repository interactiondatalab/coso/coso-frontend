import { useState, useEffect, useContext } from 'react';
import { UserContext } from "../../utils/UserContext"
import { Message, Button, Grid } from 'semantic-ui-react';
import Api from '../../Api';

function SlackMessage () {
  const [visible, setVisible] = useState(false);
  const [exists, setExists] = useState(false);
  const [link, setLink] = useState("");
  const [loading, setLoading] = useState(true);
  const userContext = useContext(UserContext);

  useEffect(() => {
    if (visible & !exists) {
      if (userContext.user.username) {
        setLoading(true);
        Api.get('/slack/exists')
        .then(res => {
          setExists(true);
          setLoading(false);
        })
        .catch(error => {
          setLoading(false);
          console.log(error);
        })
      }
    }
  }, [exists, userContext, visible])

  useEffect(() => {
    if (link === "") {
      if (userContext.user.username) {
        setLoading(true);
        Api.get('/auth/slack')
        .then(res => {
          setLink(res.data.url);
          setLoading(false);
        })
        .catch(error => {
          console.log(error);
          setLoading(false);
        })
      }
    }
  }, [link, userContext])

  useEffect(() => {
    console.log(userContext.user)
    if (userContext.user.username) {
      setVisible(true)
    }
  }, [userContext.user])

  if (visible & !exists) {
    return (
      <Grid.Column>
      <Message attached onDismiss={() => (setVisible(false))} info visible>
        <Message.Header>Does your team use Slack?</Message.Header>
        <p>Unlock more insights by linking your Slack channels with CoSo!</p>
        <Button loading={loading} as='a' href={link}>Add Slack App!</Button>
      </Message>
      </Grid.Column>
    )
  }
  else {
    return (null)
  }
}

export default SlackMessage;
