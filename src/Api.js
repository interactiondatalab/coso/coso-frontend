import axios from 'axios';

let instance = axios.create({
  baseURL: `https://coso-backend.herokuapp.com/api/`
  // baseURL: `http://localhost:3000/api/`
})


/**
 *
 * parse error response
 */
function parseError (messages) {
  // error
  if (messages) {
    if (messages instanceof Array) {
      return Promise.reject({ messages: messages })
    } else {
      return Promise.reject({ messages: [messages] })
    }
  } else {
    return Promise.reject({ messages: ['No messages'] })
  }
}


/**
 * parse response
 */
function parseBody (response) {
//  if (response.status === 200 && response.data.status.code === 200) { // - if use custom status code
  return response

  // if (response.status === 200) {
  //   return response
  // } else {
  //   return parseError(response.data.messages)
  // }
}

instance.interceptors.request.use((config) => {
  const apiToken = sessionStorage.getItem("Authorization") || localStorage.getItem("Authorization") || ''
  config.headers = { 'Authorization': apiToken }
  return config
}, error => {
  return Promise.reject(error)
})

instance.interceptors.response.use((response) => {
  return parseBody(response)
}, error => {
  if (error.response) {
    if (error.response.status === 401) {
      sessionStorage.removeItem("Authorization")
      sessionStorage.removeItem("User")
      localStorage.removeItem("Authorization")
      localStorage.removeItem("User")
    }
    return parseError(error.response.data)
  } else {
    return Promise.reject(error)
  }
})


export default instance;
