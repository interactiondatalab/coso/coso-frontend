import React from 'react'

import { Button, Form, Header, Icon, Modal, Segment, Message } from 'semantic-ui-react'

import TeamSelect from './TeamSelect'
import IgemUserIDSelect from './IgemUserIDSelect'

import Api from '../Api'

class SignUpModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
                  open: false,
                  email: "",
                  password: "",
                  username: "",
                  team_id: "",
                  error: "",
                  fullName: "",
                  username_exists: true,
                  success: false
                };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleDismiss = this.handleDismiss.bind(this);
    this.setTeamId = this.setTeamId.bind(this);
  }

  handleDismiss(event) {
    this.setState({error: "", success: false})
  }

  handleChange(event) {
    if (event.target.id === "email") {
      this.setState({email: event.target.value});
    }
    if (event.target.id === "password") {
      this.setState({password: event.target.value});
    }
    if (event.target.id === "fullName") {
      this.setState({fullName: event.target.value});
    }
    if (event.target.id === "username") {
      console.log(event)
      this.setState({username: event.target.value});
    }
  }

  setTeamId(value) {
    this.setState({team_id: value});
  }

  handleSubmit(event) {
    event.preventDefault();

    const user = {
        'email': this.state.email,
        'password': this.state.password,
        'username': this.state.username,
        'team_id': this.state.team_id,
        'full_name': this.state.fullName
    }
    Api.post('signup', {user})
    .then(res => {
      this.setState({success: true });
    })
    .catch(error => {
      console.log(error)
      if (error.messages) {
        this.setState({error: error.messages[0].error});
      }
    })
  }

  render() {
    return (

      <Modal
        closeIcon
        dimmer="blurring"
        size="tiny"
        open={this.state.open}
        trigger={this.props.children}
        onClose={() => this.setState({open: false})}
        onOpen={() => this.setState({open: true})}
      >

        <Header icon='user' content='Create an account' />
        <Form>
          <Modal.Content>
          <Segment stacked>
            <Header as="h3">Account information</Header>
            <Form.Input
               id="email"
               fluid
               icon='user'
               iconPosition='left'
               placeholder='E-mail address'
               onChange={this.handleChange}
            />
            <Form.Input
               id="password"
               fluid
               icon='lock'
               iconPosition='left'
               placeholder='Password'
               type='password'
               onChange={this.handleChange}
             />
             <Header as="h3">Igem Team</Header>
             <TeamSelect setTeamId={this.setTeamId} />
             <Header as="h3">Igem (Wiki) Username</Header>
             <IgemUserIDSelect teamId={this.state.team_id} setIgemUserId={this.handleChange} />
              <Message
                warning
                icon="meh"
                header="Hum..."
                visible={!this.state.username_exists}
                content="It looks like this username does not exists in the iGEM database? If you are not registered yet in the iGEM team roaster, please ignore this warning and continue with Sign-Up!"
              />
              <Header as="h3">Your name</Header>
              <Form.Input
                 id="fullName"
                 fluid
                 icon='user'
                 iconPosition='left'
                 placeholder='Your Name'
                 onChange={this.handleChange}
              />
           </Segment>
          </Modal.Content>
            { this.state.success ? <Message
                                    success
                                    onDismiss={this.handleDismiss}
                                    icon="checkmark box"
                                    visible
                                    header='Your user registration was successful'
                                    content='You may now log-in with the username you have chosen'
                                  /> : null }
            { this.state.error ? <Message
                                    negative
                                    onDismiss={this.handleDismiss}
                                    icon="frown"
                                    header="Oh oh"
                                    visible
                                    content={this.state.error}
                                  /> : null }
            { this.state.success ?
              <Button color='grey' fluid size='large' onClick={() => {this.setState({open: false})}}>
                <Icon name='checkmark' /> Close
              </Button>
              :
              <Button color='teal' fluid size='large' onClick={this.handleSubmit}>
                <Icon name='checkmark' /> Sign-Up
              </Button>
            }
          </Form>
      </Modal>


    );
  }
}

export default SignUpModal
