import React from 'react'

import { Button, Form, Header, Icon, Modal, Segment, Message } from 'semantic-ui-react'
import {UserContext} from '../utils/UserContext';
import Api from '../Api'

class LogInModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
                  open: false,
                  email: "",
                  password: "",
                  error: "",
                  success: false,
                  rememberMe: true
                };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleDismiss = this.handleDismiss.bind(this);
  }

  handleDismiss(event) {
    this.setState({error: "", success: false})
  }


  handleChange(event) {
    if (event.target.id === "email") {
      this.setState({email: event.target.value});
    }
    if (event.target.id === "password") {
      this.setState({password: event.target.value});
    }
  }

  handleSubmit(event) {
    event.preventDefault();

    const user = {
        'email': this.state.email,
        'password': this.state.password
    }

    sessionStorage.removeItem("Authorization")
    sessionStorage.removeItem("User")

    Api.post('login', {user})
    .then((res) => {
      if (this.state.rememberMe) {
        localStorage.setItem("Authorization", res.headers.authorization);
        localStorage.setItem("User", JSON.stringify(res.data));
      } else {
        sessionStorage.setItem("Authorization", res.headers.authorization);
        sessionStorage.setItem("User", JSON.stringify(res.data));
      }
      this.context.setUser(res.data);
      this.setState({open: false});
    })
    .catch((error) => {
      console.log(error)
      if (error.messages) {
        this.setState({error: error.messages[0].error});
      }
      else {
        this.setState({error: "Unknown error..."})
      }

    })
  }

  render() {
    return (

      <Modal
        closeIcon
        dimmer="blurring"
        size="tiny"
        open={this.state.open}
        trigger={this.props.children}
        onClose={() => this.setState({open: false})}
        onOpen={() => this.setState({open: true})}
      >

        <Header icon='user' content='Please Log In to your account' />
        <Form>
          <Modal.Content>
          <Segment stacked>
            <Form.Input
               id="email"
               fluid
               icon='user'
               iconPosition='left'
               placeholder='E-mail address'
               onChange={this.handleChange}
            />
            <Form.Input
               id="password"
               fluid
               icon='lock'
               iconPosition='left'
               placeholder='Password'
               type='password'
               onChange={this.handleChange}
             />
             <Form.Checkbox
               toggle
               label='Remember me'
               onChange={() => {this.setState({rememberMe: !this.state.rememberMe})}}
               checked={this.state.rememberMe}
              />
           </Segment>
          </Modal.Content>
            { this.state.success ? <Message
                                    success
                                    icon="checkmark box"
                                    header='LogIn Success'
                                    content='Redirecting you ...'
                                  /> : null }
            { this.state.error ? <Message
                                    negative
                                    onDismiss={this.handleDismiss}
                                    icon="frown"
                                    header="Oh oh"
                                    content={this.state.error}
                                  /> : null }
            <Button color='teal' fluid size='large' onClick={this.handleSubmit}>
              <Icon name='checkmark' /> Log-In
            </Button>
          </Form>
      </Modal>


    );
  }
}

LogInModal.contextType = UserContext;

export default LogInModal
