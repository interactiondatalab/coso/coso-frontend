import { Container, Divider, Segment, Grid, Header, Button } from 'semantic-ui-react';
import LogInModal from "../utils/LogInModal";
import SignUpModal from "../utils/SignUpModal";

const NotLoggedIn = () => {
  return (
    <Container className="main-container">
      <Segment basic textAlign="center">
        <Header as="h1">
          You are not logged in!
        </Header>
        In order to access your team's metadata and information you need to first Log In or Sign Up.
      </Segment>
      <Segment basic>
        <Grid columns={2} stackable textAlign='center'>
          <Divider vertical>Or</Divider>
          <Grid.Row verticalAlign='middle'>
            <Grid.Column>
              <LogInModal>
                <Button color='teal'>Log-In</Button>
              </LogInModal>
            </Grid.Column>

            <Grid.Column>
              <SignUpModal>
                <Button primary>Sign Up</Button>
              </SignUpModal>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Segment>
    </Container>
  )
}

export default NotLoggedIn;
