import React, { useState, useEffect } from 'react'

import { Form, Dropdown, Message, Segment } from 'semantic-ui-react'

import Api from '../Api'

function IgemUserIDSelect({teamId, setIgemUserId, save}) {
  const [users, setUsers] = useState([]);
  const [searchQuery, setSearchQuery] = useState('');
  const [value, setValue] = useState('');
  const [loading, setLoading] = useState(true);
  const [disabled, setDisabled] = useState(true);
  const [username_exists, setUsername_exists] = useState(true);

  useEffect(() => {
    if (teamId) {
      setDisabled(false);
    }
  }, [teamId])

  useEffect(() => {
    if (teamId) {
      Api.get('igem_users', {
            params: {
              team_id: teamId
            }
          })
      .then(res => {
        var i = 0;
        var options = []
        res.data.forEach(element => {
          if (element.user === null && element.custom === false) {
            if (save) {
              options.push({
                key: i,
                text: element.username,
                value: element.id
              });
            } else {
              options.push({
                key: i,
                text: element.username,
                value: element.username
              });
            }
            i+=1;
          }
        })
        setUsers(options);
        setLoading(false);
      })
      .catch(error => {
        console.log("ERROR on Team index");
        console.log(error);
      })
    }
  }, [teamId, save])

  function handleChange (e, { searchQuery, value }) {
    console.log(searchQuery)
    setValue(value)
    users.forEach((item, i) => {
      if (item.value === value) {
        setSearchQuery(item.text)
      }
    });
    if (save) {
      setIgemUserId(value)
    } else {
      setIgemUserId({target: {id: "username", value: value}})
    }
  }

  function handleSearchChange (e, { searchQuery }) {
    setSearchQuery(searchQuery)
    Api.get("/users/exist", {params: {"username": searchQuery}})
    .then(res => {
      setUsername_exists(true)
    })
    .catch(error => {
      setUsername_exists(false)
    })
    if (!save) {
      setIgemUserId({target: {id: "username", value: searchQuery}});
    }
  }

  if (save) {
    return (
      <Dropdown
        button
        className='icon'
        floating
        labeled
        icon='save'
        options={users}
        loading={loading}
        search
        onChange={handleChange}
        placeholder='Select Username'
        value={value}
      />
    )
  } else {
    return (
      <>
        <Segment basic attached>
            <Form.Dropdown
              placeholder='Igem UserName'
              fluid
              search
              selection
              options={users}
              loading={loading}
              disabled={disabled}
              id="username"
              onChange={handleChange}
              onSearchChange={handleSearchChange}
              searchQuery={searchQuery}
              value={value}
            />
        </Segment>
        <Message
          attached='bottom'
          warning
          icon="meh"
          header="Hum..."
          visible={!username_exists}
          content="It looks like this username does not exists in the iGEM database? If you are not registered yet in the iGEM team roaster, please ignore this warning and continue with Sign-Up!"
        />
      </>
    )
  }
}

export default IgemUserIDSelect
