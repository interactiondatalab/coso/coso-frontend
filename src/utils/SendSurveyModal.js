import React, { useState, useEffect } from 'react';

import { clone } from "./Functions"
// import { UserContext } from '../utils/UserContext';

import { Modal, Header, Icon, Segment, Button, Form, Grid, Divider, Label } from 'semantic-ui-react'
import { Survey } from "../components/Surveys"
import Api from '../Api'
import TeamSelect from "./TeamSelect"

const FormFieldSelect = ( { survey_field_id, setValue} ) => {

  const fieldTypes = [
    {
      key: 'inputCheckbox',
      text: 'Checkbox',
      value: 'inputCheckbox',
    },
    {
      key: 'inputDate',
      text: 'Year',
      value: 'inputDate',
    },
    {
      key: 'inputField',
      text: 'Single field',
      value: 'inputField',
    },
    {
      key: 'inputTextfield',
      text: 'TextArea',
      value: 'inputTextfield',
    },
    {
      key: 'inputSelect',
      text: 'Dropdown',
      value: 'inputSelect',
    },
    {
      key: 'inputSlider',
      text: 'Slider',
      value: 'inputSlider',
    },
    {
      key: 'inputRadioList',
      text: 'Radio list',
      value: 'inputRadioList',
    },
    {
      key: 'inputRadioButton',
      text: 'Radio boolean',
      value: 'inputRadioButton',
    },
    {
      key: 'inputRadioMatrix',
      text: 'Radio matrix',
      value: 'inputRadioMatrix',
    },
    {
      key: 'listNames',
      text: 'Team users',
      value: 'listNames',
    }
  ]

  return (
    <Form.Dropdown
      placeholder='Select Field'
      fluid
      selection
      options={fieldTypes}
      id={survey_field_id}
      onChange={(e, {value}) => {setValue(value)}}
    />
  )
}

const FormInputDate = ({setText, setName}) => (
  <>
    <Form.Input
      label='Title'
      placeholder='A great question title.'
      onChange={(e, {value}) => setName(value)}
    />
    <Form.Input
      label='Description'
      placeholder='A great question description.'
      onChange={(e, {value}) => setText(value)}
    />
  </>
)

const FormInputField = ({setText, setName, setPlaceholder}) =>  (
  <>
    <Form.Input
      label='Title'
      placeholder='A great question title.'
      onChange={(e, {value}) => setName(value)}
    />
    <Form.Input
      label='Description'
      placeholder='A great question description.'
      onChange={(e, {value}) => setText(value)}
    />
    <Form.Input
      label='Placeholder'
      placeholder='A great placeholder.'
      onChange={(e, {value}) => setPlaceholder(value)}
    />
  </>
)

const FormInputSelect = ({setText, setName, setPlaceholder, setOptions}) => {
  const [radios, setRadios] = useState([
    {"label":"Label 1","value":"label1"},
    {"label":"Label 2","value":"label2"}
  ])

  const [fields, setFields] = useState(false);
  const [refresh, setRefresh] = useState(false);

  useEffect(() => {
    setOptions(radios)
  }, [radios])

  useEffect(() => {
    console.log("refreshed")
    console.log({radios})
    const tmp = radios.map((item, i) => {
      return (
        <Form.Group widths='equal' key={i}>
          <Form.Input width={7} label="Label" placeholder="A label" onChange={(e, {value}) => {radios[i].label = value; radios[i].value = value; setRadios(radios); setRefresh(!refresh);}}/>
          <Form.Button width={2} onClick={() => {radios.splice(i, 1); setRadios(radios); console.log(radios); setRefresh(!refresh);}} icon><Icon name="trash"/></Form.Button>
        </Form.Group>
      )
    })
    setFields(tmp);
  }, [refresh, radios])

  return (
  <>
    <Form.Input
      label='Title'
      placeholder='A great question title.'
      onChange={(e, {value}) => setName(value)}
    />
    <Form.Input
      label='Description'
      placeholder='A great question description.'
      onChange={(e, {value}) => setText(value)}
    />
    <Form.Input
      label='Placeholder'
      placeholder='A great placeholder.'
      onChange={(e, {value}) => setPlaceholder(value)}
    />
    {
      fields
    }
    <Button onClick={() => {console.log("add"); radios.push({"label":"Label","value":"value"}); setRadios(radios); setRefresh(!refresh)}} icon><Icon name="add"/></Button>
  </>
)}

const FormInputTextfield = ({setText, setName, setPlaceholder}) =>  (
  <>
    <Form.Input
      label='Title'
      placeholder='A great question title.'
      onChange={(e, {value}) => setName(value)}
    />
    <Form.Input
      label='Description'
      placeholder='A great question description.'
      onChange={(e, {value}) => setText(value)}
    />
    <Form.Input
      label='Placeholder'
      placeholder='A great placeholder.'
      onChange={(e, {value}) => setPlaceholder(value)}
    />
  </>
)

const FormInputSlider = ({setName, setContent}) => {

  const [leftLabel, setLeftLabel] = useState("");
  const [rightLabel, setRightLabel] = useState("");
  const [min, setMin] = useState(0);
  const [step, setStep] = useState(1);
  const [max, setMax] = useState(5);
  const [text, setText] = useState("");

  useEffect(() => {
    const content = {
      "text":text,
      "max":parseFloat(max),
      "min":parseFloat(min),
      "step":parseFloat(step),
      "labels": {
        "min": leftLabel,
        "max": rightLabel
      }
    }
    setContent(content)
  }, [leftLabel, rightLabel, min, max, step])

  return (
    <>
      <Form.Input
        label='Title'
        placeholder='A great question title.'
        onChange={(e, {value}) => setName(value)}
      />
      <Form.Input
        label='Description'
        placeholder='A great question description.'
        onChange={(e, {value}) => setText(value)}
      />
      <Form.Group width="equal">
        <Form.Input
          width={8}
          label='Left label'
          placeholder='Not at all'
          onChange={(e, {value}) => setLeftLabel(value)}
        />
        <Form.Input
          width={8}
          label='Right Label'
          placeholder='A lot'
          onChange={(e, {value}) => setRightLabel(value)}
        />
      </Form.Group>
      <Form.Group width="equal">
        <Form.Input
          width={5}
          label='Min'
          placeholder='0'
          onChange={(e, {value}) => setMin(value)}
        />
        <Form.Input
          width={5}
          label='Step'
          placeholder='0.5'
          onChange={(e, {value}) => setStep(value)}
        />
        <Form.Input
          width={5}
          label='Max'
          placeholder='4'
          onChange={(e, {value}) => setMax(value)}
        />
      </Form.Group>
    </>
  )
}

const FormInputCheckbox = ({setText, setName, setOptions}) => {
  const [radios, setRadios] = useState([
    {"label":"Label 1"},
    {"label":"Label 2"}
  ])

  const [fields, setFields] = useState(false);
  const [refresh, setRefresh] = useState(false);

  useEffect(() => {
    setOptions(radios)
  }, [radios])

  useEffect(() => {
    console.log("refreshed")
    console.log({radios})
    const tmp = radios.map((item, i) => {
      return (
        <Form.Group widths='equal' key={i}>
          <Form.Input width={7} label="Label" placeholder="A label" onChange={(e, {value}) => {radios[i].label = value; setRadios(radios); setRefresh(!refresh);}}/>
          <Form.Button width={2} onClick={() => {radios.splice(i, 1); setRadios(radios); console.log(radios); setRefresh(!refresh);}} icon><Icon name="trash"/></Form.Button>
        </Form.Group>
      )
    })
    setFields(tmp);
  }, [refresh, radios])

  return (
  <>
    <Form.Input
      label='Title'
      placeholder='A great question title.'
      onChange={(e, {value}) => setName(value)}
    />
    <Form.Input
      label='Description'
      placeholder='A great question description.'
      onChange={(e, {value}) => setText(value)}
    />
    {
      fields
    }
    <Button onClick={() => {console.log("add"); radios.push({"label":"Label"}); setRadios(radios); setRefresh(!refresh)}} icon><Icon name="add"/></Button>
  </>
)}

const FormInputRadioList = ({setText, setName , setOptions}) => {
  const [radios, setRadios] = useState([
    {"label":"Label 1","value":"label1"},
    {"label":"Label 2","value":"label2"}
  ])

  const [fields, setFields] = useState(false);
  const [refresh, setRefresh] = useState(false);

  useEffect(() => {
    setOptions(radios)
  }, [radios])

  useEffect(() => {
    console.log("refreshed")
    console.log({radios})
    const tmp = radios.map((item, i) => {
      return (
        <Form.Group widths='equal' key={i}>
          <Form.Input width={7} label="Label" placeholder="A label" onChange={(e, {value}) => {radios[i].label = value; radios[i].value = value; setRadios(radios); setRefresh(!refresh);}}/>
          <Form.Button width={2} onClick={() => {radios.splice(i, 1); setRadios(radios); console.log(radios); setRefresh(!refresh);}} icon><Icon name="trash"/></Form.Button>
        </Form.Group>
      )
    })
    setFields(tmp);
  }, [refresh, radios])

  return (
  <>
    <Form.Input
      label='Title'
      placeholder='A great question title.'
      onChange={(e, {value}) => setName(value)}
    />
    <Form.Input
      label='Description'
      placeholder='A great question description.'
      onChange={(e, {value}) => setText(value)}
    />
    {
      fields
    }
    <Button onClick={() => {console.log("add"); radios.push({"label":"Label","value":"value"}); setRadios(radios); setRefresh(!refresh)}} icon><Icon name="add"/></Button>
  </>
)}

const FormInputRadioButton = ({setText, setName , setOptions}) => {
  const [left, setLeft] = useState("")
  const [right, setRight] = useState("")

  useEffect(() => {
    setOptions([
      {"label":left,"value":left},
      {"label":right,"value":right}
    ])
  }, [left, right])

  return (
  <>
    <Form.Input
      label='Title'
      placeholder='A great question title.'
      onChange={(e, {value}) => setName(value)}
    />
    <Form.Input
      label='Description'
      placeholder='A great question description.'
      onChange={(e, {value}) => setText(value)}
    />
    <Form.Group width="equal">
      <Form.Input
        label='Left'
        placeholder='A simple word'
        onChange={(e, {value}) => setLeft(value)}
      />
      <Form.Input
        label='Right'
        placeholder='A simple word'
        onChange={(e, {value}) => setRight(value)}
      />
    </Form.Group>
  </>
)}

const FormInputRadioMatrix = ({setText, setName, setOptions}) => {
  const [cols, setCols] = useState([
    {"label":"", "sublabel":  "", "value":"1"},
    {"label":"", "sublabel": "", "value":"2"},
    {"label":"", "sublabel": "", "value":"3"},
    {"label":"", "sublabel": "", "value":"4"},
    {"label":"", "sublabel": "", "value":"5"}
  ])

  const [rows, setRows] = useState([
    {"label":"Change me !"},
    {"label":"Change me !"},
  ]);

  const [colsFields, setColsFields] = useState([]);
  const [rowsFields, setRowsFields] = useState([]);
  const [refresh, setRefresh] = useState(false);

  useEffect(() => {
    const options = {
      columns: cols,
      rows: rows,
    }
    setOptions(options)
  }, [rows, cols, refresh])

  useEffect(() => {
    const tmp = cols.map((item, i) => {
      return (
        <Form.Group widths='equal' key={i}>
          <Form.Input width={8} label={i === 0 ? "Label" : null} placeholder="A label" onChange={(e, {value}) => {cols[i].label = value; setCols(cols); setRefresh(!refresh);}}/>
          <Form.Input width={8} label={i === 0 ? "Tag" : null} placeholder="A Tag" onChange={(e, {value}) => {cols[i].sublabel = value; setCols(cols); setRefresh(!refresh);}}/>
        </Form.Group>
      )
    })
    setColsFields(tmp);
  }, [refresh, cols])

  useEffect(() => {
    const tmp = rows.map((item, i) => {
      return (
        <Form.Group widths='equal' key={i}>
          <Form.Input label="Question" placeholder="A question" onChange={(e, {value}) => {rows[i].label = value; setRows(rows); setRefresh(!refresh);}}/>
          <Form.Button onClick={() => {rows.splice(i, 1); setRows(rows); setRefresh(!refresh);}} icon><Icon name="trash" /></Form.Button>
        </Form.Group>
      )
    })
    setRowsFields(tmp);
  }, [refresh, rows])

  // return (null)

  return (
  <>
    <Form.Input
      label='Title'
      placeholder='A great question title.'
      onChange={(e, {value}) => setName(value)}
    />
    <Form.Input
      label='Description'
      placeholder='A great question description.'
      onChange={(e, {value}) => setText(value)}
    />
    <Label basic content="Options"/>
    {
      colsFields
    }
    <Label basic content="Questions"/>
    {
      rowsFields
    }
    <Button onClick={() => {console.log("add"); rows.push({"label":"Change me !"}); setRows(rows); setRefresh(!refresh)}} icon><Icon name="add"/></Button>
  </>
  )
}

const FormListNames = ( { setText, setName }) => (
  <>
    <Form.Input
      label='Title'
      placeholder='A great question title.'
      onChange={(e, {value}) => setName(value)}
    />
    <Form.Input
      label='Description'
      placeholder='A great question description.'
      onChange={(e, {value}) => setText(value)}
    />
  </>
)

const SendSurveyModal = ( props ) => {
  const [open, setOpen] = useState(false);
  const [refresh, setRefresh] = useState(false);
  const [survey, setSurvey] = useState({name:"", description:"", time:null, survey_fields:[]});
  const [surveyForm, setSurveyForm] = useState(null);
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");

  const [loading, setLoading] = useState(false)

  const [teamId, setTeamId] = useState(null)
  const [clearTeamId, setClearTeamId] = useState(false)

  useEffect(() => {
    survey.team_id = teamId;
    saveSurvey(survey)
  }, [teamId])

  useEffect(() => {
    console.log("Survey update trigger")
    console.log(survey)
  }, [survey])

  useEffect(() => {
    // const tmp = clone(survey)
    const tmp = survey
    tmp.name = title
    setSurvey(tmp)
    setRefresh(!refresh);
    console.log(survey)
  }, [title])

  useEffect(() => {
    // const tmp = clone(survey)
    const tmp = survey
    tmp.description = description
    setSurvey(tmp)
    setRefresh(!refresh);
  }, [description])

  function onSend() {
    setLoading(true)
    Api.post("surveys/", {survey})
    .then(res => {
      console.log(res)
    })
    .catch(error => {
      console.log(error)
    })
    .then(() => {
      props.setRefresh(true)
      setLoading(false)
      setOpen(false)
    });
  }

  function clearTeam() {
    setTeamId(null)
    setClearTeamId(true)
    setTimeout(function () {
        setClearTeamId(false)
    }, 50);
  }

  function saveSurvey(survey) {
    setSurvey(survey);
    setRefresh(!refresh);
  }

  function setCategory(i, value) {
    survey.survey_fields[i].category = value;
    if (value == "inputRadioMatrix") {
      survey.survey_fields[i].content.options.rows = []
      survey.survey_fields[i].content.options.columns = []
    } else {
      survey.survey_fields[i].content = {text:"", options:[]}
    }
    saveSurvey(survey);
  }

  function setName(i, value) {
    survey.survey_fields[i].name = value;
    saveSurvey(survey);
  }

  function setText(i, value) {
    survey.survey_fields[i].content.text = value;
    saveSurvey(survey);
  }

  function setPlaceholder(i, value) {
    survey.survey_fields[i].content.placeholder = value;
    saveSurvey(survey);
  }

  function setOptions(i, value) {
    survey.survey_fields[i].content.options = value;
    saveSurvey(survey);
  }

  function setContent(i, value) {
    survey.survey_fields[i].content = value;
    saveSurvey(survey);
  }

  function setRequired(i) {
    survey.survey_fields[i].required = !survey.survey_fields[i].required;
    saveSurvey(survey);
  }

  function addField() {
    survey.survey_fields.push({name: "", category: "", required: false, content: {text: "", placeholder:"", options:[]}});
    saveSurvey(survey);
  }

  function removeField(index) {
    survey.survey_fields.splice(index);
    saveSurvey(survey);
  }

  useEffect(() => {
    const tmp = survey.survey_fields.map((item, i) => {
      return (
        <>
          <Divider />
          <Form.Group widths='equal'>
            <FormFieldSelect survey_field_id={i} setValue={(value) => {setCategory(i, value)}}/>
            <Button icon onClick={() => {removeField(i)}}>
              <Icon name='trash' />
            </Button>
            <Button icon toggle active={item.required} onClick={() => {setRequired(i)}}>
              <Icon name='check' />
            </Button>
          </Form.Group>
          {item.category === "inputDate" && <FormInputDate  setText={(value) => {setText(i, value)}} setName={(value) => {setName(i, value)}}  />}
          {item.category === "inputRadioMatrix" && <FormInputRadioMatrix setText={(value) => {setText(i, value)}} setName={(value) => {setName(i, value)}} setOptions={(value)=>{setOptions(i, value)}} />}
          {item.category === "inputRadioList" && <FormInputRadioList setText={(value) => {setText(i, value)}} setName={(value) => {setName(i, value)}} setOptions={(value)=>{setOptions(i, value)}} />}
          {item.category === "inputCheckbox" && <FormInputCheckbox setText={(value)=>{setText(i, value)}} setName={(value)=>{setName(i, value)}} setOptions={(value)=>{setOptions(i, value)}} />}
          {item.category === "inputRadioButton" && <FormInputRadioButton setText={(value)=>{setText(i, value)}} setName={(value)=>{setName(i, value)}} setOptions={(value)=>{setOptions(i, value)}} />}
          {item.category === "inputSlider" && <FormInputSlider setText={(value)=>{setText(i, value)}} setName={(value)=>{setName(i, value)}} setContent={(value)=>{setContent(i, value)}} />}
          {item.category === "inputTextfield" && <FormInputTextfield  setText={(value)=>{setText(i, value)}} setName={(value)=>{setName(i, value)}} setPlaceholder={(value)=>{setPlaceholder(i, value)}}/>}
          {item.category === "inputField" && <FormInputField  setText={(value)=>{setText(i, value)}} setName={(value)=>{setName(i, value)}} setPlaceholder={(value)=>{setPlaceholder(i, value)}}/>}
          {item.category === "inputSelect" && <FormInputSelect setText={(value)=>{setText(i, value)}} setName={(value)=>{setName(i, value)}} setPlaceholder={(value)=>{setPlaceholder(i, value)}} setOptions={(value)=>{setOptions(i, value)}} />}
          {item.category === "listNames" && <FormListNames  setText={(value)=>{setText(i, value)}} setName={(value)=>{setName(i, value)}} />}
        </>
      )
    })
    setSurveyForm(tmp);
  }, [survey, refresh])

  return (
    <Modal
      closeIcon
      closeOnDimmerClick={false}
      size="medium"
      open={open}
      trigger={<Button primary>Create Survey</Button>}
      onClose={() => setOpen(false)}
      onOpen={() => setOpen(true)}
    >
      <Header icon='clipboard list' content='Create a survey' />
      <Modal.Content>
        <Grid columns={2} stackable>
          <Grid.Column>
            <Segment basic>
              <Form>
                <Form.Field label='OPTIONAL: You can restrict the survey to one team only. IF LEFT BLANK the survey will be sent to all teams.' />
                <Form.Group widths='equal'>
                  <TeamSelect clear={clearTeamId} setTeamId={setTeamId}/>
                  <Button icon="close" color="grey" onClick={clearTeam}>Clear</Button>
                </Form.Group>
                <Form.Input
                  label='Title'
                  placeholder='A great new title.'
                  onChange={(e, {value}) => setTitle(value)}
                />
                <Form.TextArea
                  fluid
                  label='Description'
                  placeholder='Some interesting description.'
                  onChange={(e, {value}) => setDescription(value)}
                />
                {surveyForm}
                <Form.Field>
                  <Button onClick={addField}>Add</Button>
                </Form.Field>
              </Form>
            </Segment>
          </Grid.Column>
          <Grid.Column>
            <Segment>
              <Header as="h3" icon='wordpress forms' content='Preview' />
              <Survey survey={survey} />
            </Segment>
          </Grid.Column>
        </Grid>
      </Modal.Content>
      <Modal.Actions>
        <Button color='red' inverted onClick={() => setOpen(false)}>
          <Icon name='remove' /> Cancel
        </Button>
        <Button color='green' inverted loading={loading} onClick={onSend}>
          <Icon name='checkmark' /> Send
        </Button>
      </Modal.Actions>
    </Modal>
  )
}

export default SendSurveyModal
