import React, { useContext, useState } from 'react';
import { useHistory } from "react-router-dom";

import { UserContext } from '../utils/UserContext';

import { Modal, Header, Icon, Segment, Button } from 'semantic-ui-react'

import Api from '../Api'

function DeleteAccountModal() {
  const [open, setOpen] = useState(false);
  const userContext = useContext(UserContext);
  const [loading, setLoading] = useState(false)
  const history = useHistory();

  function onDelete() {
    setLoading(true)
    Api.delete("users/"+userContext.user.id)
    .then(res => {
      console.log(res)
    })
    .catch(error => {
      console.log(error)
    })
    .then(() => {
      userContext.setUser({})
      setLoading(false)
      setOpen(false)
      history.push("/");
    });
  }

  return (
    <Modal
      basic
      onClose={() => setOpen(false)}
      onOpen={() => setOpen(true)}
      open={open}
      size='small'
      trigger={<Button color='red'>Delete Account</Button>}
    >
      <Header icon>
        <Icon name='archive' />
        Delete my account
      </Header>
      <Modal.Content>
        <Segment basic loading={loading}>
          This is irreversible, once you've deleted your account we won't keep any of your personal information.
        </Segment>
      </Modal.Content>
      <Modal.Actions>
        <Button basic color='red' inverted onClick={() => setOpen(false)}>
          <Icon name='remove' /> No
        </Button>
        <Button color='green' inverted onClick={onDelete}>
          <Icon name='checkmark' /> Yes
        </Button>
      </Modal.Actions>
    </Modal>
  )
}

export default DeleteAccountModal
