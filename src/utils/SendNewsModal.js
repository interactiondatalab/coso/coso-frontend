import React, { useState } from 'react';

// import { UserContext } from '../utils/UserContext';

import { Modal, Header, Icon, Segment, Button, Form, Grid } from 'semantic-ui-react'
import { News } from "../components/News"
import Api from '../Api'
import TeamSelect from "./TeamSelect"

function SendNewsModal( props ) {
  const [open, setOpen] = useState(false);
  // const userContext = useContext(UserContext);
  const [loading, setLoading] = useState(false)

  const [teamId, setTeamId] = useState(null)
  const [clearTeamId, setClearTeamId] = useState(false)
  const [title, setTitle] = useState("A great news title")
  const [content, setContent] = useState("Some very interesting content")
  const [image, setImage] = useState("https://react.semantic-ui.com/images/wireframe/image.png")
  const [actionText, setActionText] = useState("Check it out!")
  const [actionUrl, setActionUrl] = useState("https://igem-ties.info/visualization/")

  function onSend() {
    setLoading(true)
    const news = {
      team_id: teamId,
      title: title,
      content: content,
      image: image,
      action_text: actionText,
      action_url: actionUrl
    }
    Api.post("news/", {news})
    .then(res => {
      console.log(res)
    })
    .catch(error => {
      console.log(error)
    })
    .then(() => {
      props.setRefresh(true)
      setLoading(false)
      setOpen(false)
    });
  }

  function clearTeam() {
    setTeamId(null)
    setClearTeamId(true)
    setTimeout(function () {
        setClearTeamId(false)
    }, 50);
  }

  return (
    <Modal
      closeIcon
      dimmer="blurring"
      size="medium"
      open={open}
      trigger={<Button primary>Create News</Button>}
      onClose={() => setOpen(false)}
      onOpen={() => setOpen(true)}
    >
      <Header icon='newspaper outline' content='Create a news item' />
      <Modal.Content>
        <Grid columns={2} stackable>
          <Grid.Column>
            <Segment basic>
              <Form>
                <Form.Field label='OPTIONAL: You can restrict the news to one team only. IF LEFT BLANK the news will be sent to all teams.' />
                <Form.Group widths='equal'>
                  <TeamSelect clear={clearTeamId} setTeamId={setTeamId}/>
                  <Button icon="close" color="grey" onClick={clearTeam}>Clear</Button>
                </Form.Group>
                <Form.Input
                  label='Title'
                  placeholder='A great new title.'
                  onChange={(e, {value}) => setTitle(value)}
                />
                <Form.TextArea
                  fluid
                  label='Content'
                  placeholder='Some interesting content.'
                  onChange={(e, {value}) => setContent(value)}
                />
                <Form.Input
                  label='Image'
                  placeholder='http://Some.Image.Url/'
                  onChange={(e, {value}) => setImage(value)}
                />
                <Form.Input
                  label='Call to Action'
                  placeholder='Do it!'
                  onChange={(e, {value}) => setActionText(value)}
                />
                <Form.Input
                  label='Call to Action Link URL'
                  placeholder='http://Some.Image.Url/'
                  onChange={(e, {value}) => setActionUrl(value)}
                />
              </Form>
            </Segment>
          </Grid.Column>
          <Grid.Column>
            <Segment>
              <Header as="h3" icon='wordpress forms' content='Preview' />
              <News news={{
                title: title,
                content: content,
                image: image,
                action_text: actionText,
                action_url: actionUrl
              }} />
            </Segment>
          </Grid.Column>
        </Grid>
      </Modal.Content>
      <Modal.Actions>
        <Button color='red' inverted onClick={() => setOpen(false)}>
          <Icon name='remove' /> Cancel
        </Button>
        <Button color='green' inverted loading={loading} onClick={onSend}>
          <Icon name='checkmark' /> Send
        </Button>
      </Modal.Actions>
    </Modal>
  )
}

export default SendNewsModal
