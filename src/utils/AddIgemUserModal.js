import React, { useContext, useState } from 'react';
import { useHistory } from "react-router-dom";

import { UserContext } from '../utils/UserContext';

import { Modal, Header, Icon, Segment, Button, Form } from 'semantic-ui-react'

import Api from '../Api'

function AddIgemUserModal(props) {
  const [open, setOpen] = useState(false);
  const [username, setUsername] = useState("");
  const [usernameError, setUsernameError] = useState(false);
  const [fullname, setFullname] = useState("");
  const [fullnameError, setFullnameError] = useState(false);
  const userContext = useContext(UserContext);
  const [loading, setLoading] = useState(false)
  const history = useHistory();


  function onAdd() {
    setLoading(true)
    const params = {
      "igem_user": {
        "username": username,
        "full_name": fullname
      }
    }
    Api.post("igem_users/add", params)
    .then(res => {
      console.log(res)
    })
    .catch(error => {
      console.log(error)
    })
    .then(() => {
      setLoading(false)
      setOpen(false)
      props.refreshTeam(true);
    });
  }

  function validate() {
    let valid = true
    if (fullname === "") {
      setFullnameError(true)
      valid = false
    }
    if (username === "") {
      setUsernameError(true)
      valid = false
    }
    if (valid) {
      onAdd()
    }
  }

  return (
    <Modal
      dimmer="blurring"
      onClose={() => setOpen(false)}
      onOpen={() => setOpen(true)}
      open={open}
      size='small'
      trigger={<Button size="small" color="teal">Add member</Button>}
    >
      <Header icon>
        <Icon name='user' />
        Add a missing user
      </Header>
      <Modal.Content>
        <Segment basic loading={loading}>
          If you have a team member which does not appear in the list below, but is contributing to your team you can add them here.
        </Segment>
        <Form>
          <Form.Input
            label='iGEM Username'
            placeholder='bobdylan'
            error={usernameError}
            onChange={(e, {value}) => {setUsername(value);}}
          />
          <Form.Input
            label='Full Name'
            placeholder='Bob Dylan'
            error={fullnameError}
            onChange={(e, {value}) => {setFullname(value);}}
          />
        </Form>
      </Modal.Content>
      <Modal.Actions>
        <Button basic color='red' inverted onClick={() => setOpen(false)}>
          <Icon name='remove' /> Cancel
        </Button>
        <Button color='green' inverted onClick={validate}>
          <Icon name='checkmark' /> Add
        </Button>
      </Modal.Actions>
    </Modal>
  )
}

export default AddIgemUserModal
