import { createMedia } from '@artsy/fresnel'
import PropTypes from 'prop-types'
import React, { Component, useState, useEffect } from 'react'
import {
  Button,
  Container,
  Icon,
  Menu,
  Segment,
  Sidebar,
  Visibility,
  Dropdown
} from 'semantic-ui-react'
import { Link } from "react-router-dom";
import { UserContext } from '../utils/UserContext';
import LogInModal from "./LogInModal"
import SignUpModal from "./SignUpModal"
import { useLocation } from 'react-router-dom'
import Api from "../Api"

const { MediaContextProvider, Media } = createMedia({
  breakpoints: {
    mobile: 0,
    tablet: 768,
    computer: 1024,
  },
})

function logout(context) {
  sessionStorage.removeItem("Authorization");
  sessionStorage.removeItem("User");
  localStorage.removeItem("Authorization");
  localStorage.removeItem("User");
  context.setUser({});
}

function LoginButtons({fixed}) {
  return (
    <>
    <LogInModal>
      <Button as='a' inverted={!fixed}>
        Log in
      </Button>
    </LogInModal>
    <SignUpModal>
      <Button as='a' inverted={!fixed} primary={fixed} style={{ marginLeft: '0.5em' }}>
        Sign Up
      </Button>
    </SignUpModal>
    </>
  )
}

function UserIcon({fixed, context}) {
  const [isAdmin, setIsAdmin] = useState(false);

  useEffect(() => {
    Api.get('/users/is_admin')
    .then(res => {
      setIsAdmin(true);
    })
    .catch(error => {
      setIsAdmin(false);
    })
  });

  return (
    <>
    <Icon name="user circle outline" size="large" />
    <Dropdown text={context.user.username} pointing className='link item'>
      <Dropdown.Menu>
        <Dropdown.Header>My options</Dropdown.Header>
        <Dropdown.Item as={Link} to="/settings">Settings</Dropdown.Item>
        <Dropdown.Item as={Link} to="/team">Team settings</Dropdown.Item>
        {isAdmin ?
          <Dropdown.Item as={Link} to="/admin">Admin</Dropdown.Item>
        : null}
        <Dropdown.Divider />
        <Dropdown.Item as={Link} to="/about">How it works</Dropdown.Item>
        <Dropdown.Item as="a" href="mailto:igem-ties@cri-paris.org">Contact us</Dropdown.Item>
        <Dropdown.Divider />
        <Dropdown.Header>Authentication</Dropdown.Header>
        <Dropdown.Item><Button as="a" onClick={() => {logout(context)} }>LogOut</Button></Dropdown.Item>
      </Dropdown.Menu>
    </Dropdown>
    </>
  )
}

function LoginUserArea({fixed, context}) {
  return (
    <Menu.Item position='right'>
    {context.user.username ?
      <UserIcon fixed={fixed}  context={context} />
      :
      <LoginButtons fixed={fixed} />
    }
    </Menu.Item>
  )
}

function DesktopNavItems({fixed, context}) {
  const location = useLocation();

  return (
    <Container>
      <Menu.Item active={location.pathname === "/"} as={Link} to="/">Home</Menu.Item>
      <Menu.Item active={location.pathname.startsWith("/insights")} as={Link} to="/insights">Insights</Menu.Item>
      <Menu.Item active={location.pathname.startsWith("/team")} as={Link} to="/team">Team</Menu.Item>
      <Menu.Item active={location.pathname.startsWith("/surveys")} as={Link} to="/surveys">Surveys</Menu.Item>
      <Menu.Item active={location.pathname.startsWith("/news")} as={Link} to="/news">News</Menu.Item>
      <LoginUserArea fixed={fixed} context={context} />
    </Container>
  )
}

function MobileNavItems({handleSidebarHide, sidebarOpened}) {
  const location = useLocation();

  return (
    <Sidebar
      as={Menu}
      animation='overlay'
      inverted
      onHide={handleSidebarHide}
      vertical
      visible={sidebarOpened}
    >
      <Menu.Item active={location.pathname === "/"} as={Link} to="/">Home</Menu.Item>
      <Menu.Item active={location.pathname === "/insights"} as={Link} to="/insights">Insights</Menu.Item>
      <Menu.Item active={location.pathname === "/team"} as={Link} to="/team">Team</Menu.Item>
      <Menu.Item active={location.pathname === "/surveys"} as={Link} to="/surveys">Surveys</Menu.Item>
      <Menu.Item active={location.pathname === "/news"} as={Link} to="/news">News</Menu.Item>
      <LogInModal>
        <Menu.Item as={Button}>Log in</Menu.Item>
      </LogInModal>
      <SignUpModal>
        <Menu.Item as={Link}>Sign Up</Menu.Item>
      </SignUpModal>
    </Sidebar>
  )
}


class DesktopContainer extends Component {
  state = {}

  hideFixedMenu = () => this.setState({ fixed: false })
  showFixedMenu = () => this.setState({ fixed: true })

  render() {
    const { children } = this.props
    const { fixed } = this.state

    return (
      <Media greaterThan='mobile'>
        <Visibility
          once={false}
          onBottomPassed={this.showFixedMenu}
          onBottomPassedReverse={this.hideFixedMenu}
        >
          <Segment
            inverted
            textAlign='center'
            style={{ padding: '1em 0em' }}
            vertical
          >
            <Menu
              fixed={fixed ? 'top' : null}
              inverted={!fixed}
              pointing={!fixed}
              secondary={!fixed}
              size='large'
            >
            <DesktopNavItems fixed={fixed} context={this.context}/>
            </Menu>
          </Segment>
        </Visibility>

        {children}
      </Media>
    )
  }
}

DesktopContainer.propTypes = {
  children: PropTypes.node,
}

class MobileContainer extends Component {
  state = {}

  handleSidebarHide = () => this.setState({ sidebarOpened: false })

  handleToggle = () => this.setState({ sidebarOpened: true })

  render() {
    const { children } = this.props
    const { sidebarOpened } = this.state

    return (
      <Media as={Sidebar.Pushable} at='mobile'>
        <Sidebar.Pushable style={{"min-height": "100vh"}}>
          <MobileNavItems handleSidebarHide={this.handleSidebarHide} sidebarOpened={sidebarOpened} />

          <Sidebar.Pusher dimmed={sidebarOpened} style={{"min-height": "100vh"}}>
            <Segment
              inverted
              textAlign='center'
              style={{ padding: '1em 0em' }}
              vertical
            >
              <Container>
                <Menu inverted pointing secondary size='large'>
                  <Menu.Item onClick={this.handleToggle}>
                    <Icon name='sidebar' />
                  </Menu.Item>
                  <LoginUserArea fixed={false} context={this.context} />
                </Menu>
              </Container>
            </Segment>

            {children}
          </Sidebar.Pusher>
        </Sidebar.Pushable>
      </Media>
    )
  }
}

MobileContainer.propTypes = {
  children: PropTypes.node,
}

const ResponsiveContainer = ({ children }) => (
  /* Heads up!
   * For large applications it may not be best option to put all page into these containers at
   * they will be rendered twice for SSR.
   */
  <MediaContextProvider>
    <DesktopContainer>{children}</DesktopContainer>
    <MobileContainer>{children}</MobileContainer>
  </MediaContextProvider>
)

ResponsiveContainer.propTypes = {
  children: PropTypes.node,
}

DesktopContainer.contextType = UserContext;
MobileContainer.contextType = UserContext;

export default ResponsiveContainer
