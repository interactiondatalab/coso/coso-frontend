import React, { useState, useEffect } from 'react'

import { Form } from 'semantic-ui-react'

import Api from '../Api'

function TeamSelect({clear, setTeamId}) {
  const [teams, setTeams] = useState([]);
  const [searchQuery, setSearchQuery] = useState('');
  const [value, setValue] = useState('');
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    console.log("CLEARING")
    console.log(clear)
    if (clear) {
      setValue("")
      setSearchQuery("")
    }
  }, [clear])

  if (loading) {
    Api.get('teams/list')
    .then(res => {
      var i = 0;
      var options = []
      res.data.forEach(element => {
        options.push({
          key: i,
          text: element[1],
          value: element[0]
        });
        i+=1;
      })
      setTeams(options);
      setLoading(false);
    })
    .catch(error => {
      console.log("ERROR on Team index");
      console.log(error);
    })
  }

  function handleChange (e, { searchQuery, value }) {
    setValue(value)
    teams.forEach((item, i) => {
      if (item.value === value) {
        setSearchQuery(item.text)
      }
    });
    setTeamId(value)
  }

  function handleSearchChange (e, { searchQuery }) {
    setSearchQuery(searchQuery)
  }

  return (
    <Form.Dropdown
      placeholder='Igem Team'
      fluid
      search
      selection
      loading={loading}
      disabled={loading}
      options={teams}
      id="team_id"
      onChange={handleChange}
      onSearchChange={handleSearchChange}
      searchQuery={searchQuery}
      value={value}
    />
  )
}

export default TeamSelect
