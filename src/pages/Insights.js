import '../App.css';
import ScatterPlotDemo from "../components/dataviz/examples/ScatterPlotDemo"
import CalendarDemo from "../components/dataviz/examples/CalendarDemo"
import HeatMapDemo from "../components/dataviz/examples/HeatMapDemo"
import NetworkDemo from "../components/dataviz/examples/NetworkDemo"
import SwarmPlotDemo from "../components/dataviz/examples/SwarmPlotDemo"
import BarTaskFrequencyDemo from "../components/dataviz/examples/BarTaskFrequencyDemo"

import SlackUserMentionsChords from "../components/dataviz/SlackUserMentionsChords"
import SlackReactionCalendar from "../components/dataviz/SlackReactionCalendar"
import SlackMessagesCalendar from "../components/dataviz/SlackMessagesCalendar"

import { Container, Tab } from 'semantic-ui-react';
import NotLoggedIn from "../utils/NotLoggedIn";
import { UserContext } from "../utils/UserContext";
import {InsightsHeader} from "../components/InsightsHeader";

import { useContext } from 'react'

// HERE, you have the different dataviz, and the different TABS that they belong to !
// You just need to do two things
// first Import your new dataviz
import ExampleCalendar from "../components/dataviz/ExampleDataViz"

// Then add it to the correct tab:

const dataviz = {
  "Collaboration": [
    <SwarmPlotDemo />,
    <HeatMapDemo />,
    <CalendarDemo />
  ],
  "Temporal Activity": [
    <BarTaskFrequencyDemo />,
    <CalendarDemo />,
    <SwarmPlotDemo />,
    <ScatterPlotDemo />
  ],
  "Benchling": [
    <ScatterPlotDemo />,
    <NetworkDemo />
  ],
  "Slack": [
    <SlackUserMentionsChords />,
    <SlackReactionCalendar />,
    <SlackMessagesCalendar />,
    // Like so
    // <ExampleCalendar />
  ]
}


function Insights() {
  var allitems = []

  const userContext = useContext(UserContext);

  for (const [key, value] of Object.entries(dataviz)) {
    allitems = allitems.concat(value)
  }

  const panes = [
    {
      menuItem: "All",
      render: () => (
        <Tab.Pane attached={false}>
        {allitems}
        </Tab.Pane>
      )
    }
  ]

  for (const [key, value] of Object.entries(dataviz)) {
    panes.push({
      menuItem: key,
      render: () => (
        <Tab.Pane attached={false}>
        {value}
        </Tab.Pane>
      )
    })
  }

  if (userContext.user.username) {
    return (
      <Container>
        <InsightsHeader />
        <Tab menu={{ secondary: true, pointing: true }} panes={panes} />
      </Container>
    );
  } else {
    return (
      <NotLoggedIn />
    )
  }
}

export default Insights;
