import '../App.css';
import { Container, Header, Segment } from 'semantic-ui-react';
import React, { useContext } from 'react';
import { UserContext } from "../utils/UserContext";
import { Surveys, Survey } from "../components/Surveys";
import NotLoggedIn from "../utils/NotLoggedIn"

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useRouteMatch,
  useParams
} from "react-router-dom";

function SurveysPage() {

  const user = useContext(UserContext);
  console.log(user)


  let match = useRouteMatch();

  if (user.user.username) {
    return (
      <Container>
        <Segment basic>
          <Header textAlign="left" as="h1">
            Surveys
          </Header>
          <Switch>
            <Route path={`${match.path}/:surveyId`}>
              <Survey />
            </Route>
            <Route path={match.path}>
                By completing surveys you will help us understand the mechanics of team performance,
                so that you can be an inspiration for future iGEM teams!
                <Surveys/>
          </Route>
        </Switch>
      </Segment>
    </Container>

    )
  }
  else {
    return (
      <NotLoggedIn />
    );
  }
}

export default SurveysPage;
