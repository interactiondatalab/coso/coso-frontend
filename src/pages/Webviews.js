import '../App.css';
import { Segment } from 'semantic-ui-react';
import React, { useContext, useEffect } from 'react';
import { UserContext } from "../utils/UserContext";
import NotLoggedIn from "../utils/NotLoggedIn"
import Api from "../Api"
import { Survey } from "../components/Surveys";
import { News } from "../components/News";


import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useRouteMatch,
  useLocation
} from "react-router-dom";

function useQuery() {
  return new URLSearchParams(useLocation().search);
}

function WebViews() {

  const userContext = useContext(UserContext);
  let query = useQuery();


  localStorage.setItem("Authorization", `Bearer ${query.get("token")}`)

  useEffect(() => {
    document.body.style.backgroundColor = "#e4f5f6"
  }, [])

  useEffect(() => {
    Api.get("/users/self")
    .then(res => {
      console.log(res.data);
      userContext.setUser(res.data);
    })
    .catch(error => console.log(error))
  }, [])


  let match = useRouteMatch();

  console.log("test?")

    return (
      <Switch>
        <Route path={`${match.path}/surveys/:surveyId`}>
          <Segment basic style={{paddingTop:"30px"}}>
            <Survey webview={true}/>
          </Segment>
        </Route>
        <Route path={`${match.path}/news/:newsId`}>
          <Segment basic style={{paddingTop:"30px"}}>
            <News webview={true}/>
          </Segment>
        </Route>
      </Switch>
    )
}

export default WebViews;
