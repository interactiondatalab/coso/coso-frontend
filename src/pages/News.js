import '../App.css';
import { Container, Header, Segment, Grid, Divider, Button, Card, Message } from 'semantic-ui-react';
import React, { useEffect, useContext, useState } from 'react';
import { UserContext } from "../utils/UserContext";
import UserCard from "../components/UserCard";
import Api from "../Api";
import LogInModal from "../utils/LogInModal";
import SignUpModal from "../utils/SignUpModal";
import { NewsGroup } from "../components/News";
import NotLoggedIn from "../utils/NotLoggedIn"

function NewsPage() {

  const user = useContext(UserContext);

  if (user.user.username) {
    return (
      <Container>
        <Segment basic>
          <Header textAlign="left" as="h1">
            News
          </Header>
          You can find here information about your iGEM team, and how to link external services to this dataviz application.
          <NewsGroup />
        </Segment>
      </Container>
    )
  }
  else {
    return (
      <NotLoggedIn />
    );
  }
}

export default NewsPage;
