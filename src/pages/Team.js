import '../App.css';
import { Container,
         Header,
         Segment,
         Grid,
         Button,
         Message,
         Menu } from 'semantic-ui-react';
import React, { useContext, useState } from 'react';
import { UserContext } from "../utils/UserContext";

import SlackMessage from "../components/messages/SlackMessage"

import Apps from "../components/AppItems";
import TeamDisplay from "../components/TeamDisplay"
import NotLoggedIn from "../utils/NotLoggedIn"

<SlackMessage />


function Team() {
  const user = useContext(UserContext);
  const [activeItem, setActiveItem] = useState("team")
  const [benchlingMessage, setBenchlingMessage] = useState(false);
  const [googleMessage, setGoogleMessage] = useState(false);

  if (user.user.username) {
    return (
      <Container>
        <Segment basic>
          <Header as="h1">
            Your iGEM team informations
          </Header>
          You can find here information about your iGEM team, and how to link external services to this dataviz application.
        </Segment>
        <Grid columns="equal" stackable>
            {benchlingMessage ?
              <Grid.Column>
                <Message attached onDismiss={() => (setBenchlingMessage(false))} info visible>
                  <Message.Header>Does your team use Benchling?</Message.Header>
                  <p>Unlock more insights by linking your Benchling team account with CoSo!</p>
                  <Button>Link!</Button>
                </Message>
              </Grid.Column>
              : null
            }
            {googleMessage ?
              <Grid.Column>
                <Message attached onDismiss={() => (setGoogleMessage(false))} info visible>
                  <Message.Header>Does your team use Google Calendar?</Message.Header>
                  <p>Unlock more insights by linking your team's Google account with CoSo!</p>
                  <Button>Link!</Button>
                </Message>
              </Grid.Column>
              : null
            }
            <SlackMessage />
        </Grid>
        <Menu pointing secondary>
          <Menu.Item
            name='Team'
            active={activeItem === 'team'}
            onClick={() => {setActiveItem("team")}}
          />
          <Menu.Item
            name='Apps'
            active={activeItem === 'apps'}
            onClick={() => {setActiveItem("apps")}}
          />
        </Menu>
        {activeItem === "team" ?
        <TeamDisplay />
        :
        <Apps />}
      </Container>
    )
  }
  else {
    return (
      <NotLoggedIn />
    );
  }
}

export default Team;
