import '../App.css';
import React, {useEffect, useState} from 'react';
import { Container,
         Segment,
         Header       } from 'semantic-ui-react';
import { NewsGroup } from "../components/News";
import { useHistory } from "react-router-dom";
import Api from "../Api"


function Admin() {
  const history = useHistory();
  const [isAdmin, setIsAdmin] = useState(false);

  useEffect(() => {
    Api.get('/users/is_admin')
    .then(res => {
      setIsAdmin(true);
    })
    .catch(error => {
      if (error.status !== 403) {
        console.log(error)
      }
      history.push('/');
      setIsAdmin(false);
    })
  });

  return (
    <>
    { isAdmin ?
    <Container>
      <Segment vertical>
        <Header as="h1" content="News" />
        <NewsGroup />
      </Segment>
    </Container>
    :
    null
    }
    </>
  )
}

export default Admin;
