import '../App.css';
import React from 'react';
import { Container,
         Segment,
         Button,
         Grid,
         Divider,
         Header,
         Icon,
         Modal,
         Image        } from 'semantic-ui-react';
import {UserContext} from '../utils/UserContext';
import cosoPoster from '../images/CoSo-Flyer-V2.1.png';
import information_form from '../files/Information_form.pdf';
import igemstudy_slides from '../files/iGEM_study_description_July_2019.pdf';
import marc_image from '../images/Marc.jpg';
import leo_image from '../images/Leo.jpg';
import lionel_image from '../images/Lionel.jpg';
import raphael_image from '../images/Raphael.jpg';
import rathin_image from '../images/Rathin.jpg';
import robbie_image from '../images/Robbie.jpg';
import romain_image from '../images/Romain.jpg';
import savvy_image from '../images/Savvy.jpg';

const ModalCoSoPoster = () => {
  const [open, setOpen] = React.useState(false)

  return (
    <Modal
      open={open}
      onClose={() => setOpen(false)}
      onOpen={() => setOpen(true)}
      trigger={<Button>Learn More</Button>}
    >
      <Modal.Header>Learn more about CoSo</Modal.Header>
      <Modal.Content textAlign="center" image scrolling>
        <Image src={cosoPoster} centered fluid wrapped />
      </Modal.Content>
      <Modal.Actions>
        <Button onClick={() => setOpen(false)} primary>
          Close
        </Button>
      </Modal.Actions>
    </Modal>
  )
}


class Home extends React.Component {

  static contextType = UserContext;


  render () {
    console.log(this.context)

    return (
      <Container className="main-container">
        <Segment basic textAlign="center">
          <Header  as='h1'>Project description</Header>
        </Segment>
        <Segment vertical textAlign="left">
          <ul>
            <li>
              iGEM TIES (Team IntEraction Study) explores how iGEM team interactions, diversity and transdisciplinarity impact the global performance of iGEM teams and the learning experience of the students.
            </li>
            <li>
              Join the study and take part in the first large scale description of iGEM team work! Don't hesitate to shoot us an email at igem-ties@cri-paris.org if you have any question.
            </li>
            <li>
              All the collected data is anonymised, kept secured on a server, and participation to each data collection is completely voluntary.
            </li>
            <li>
              Participating teams will get a report of their team interactions (interaction networks, number of interactions etc) after the Wiki Freeze.
            </li>
            <li>
              You can visit this page to see visualizations of data collected during iGEM 2020.
            </li>
          </ul>
        </Segment>
        <Segment basic textAlign="center">
          <Header  as='h1'>CoSo is now available!</Header>
          To join the study, download the latest version of CoSo
        </Segment>
        <Segment basic textAlign="left">
          <Grid columns={2} stackable textAlign='center'>
            <Divider vertical>Or</Divider>
            <Grid.Row verticalAlign='middle'>
              <Grid.Column>
                <Header icon as="a" href="https://play.google.com/store/apps/details?id=com.coso" target="_blank">
                  <Icon name="android" size="massive"/>
                  Google PlayStore
                </Header>
              </Grid.Column>

              <Grid.Column>
                <Header icon as="a" href="https://apps.apple.com/app/coso/id1483601800" target="_blank">
                  <Icon name="apple" size="massive"/>
                  Apple AppStore
                </Header>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Segment>
        <Segment vertical textAlign="center">
          <ModalCoSoPoster/>
        </Segment>
        <Segment basic textAlign="center">
          <Header  as='h1'>Project details</Header>
        </Segment>
        <Segment basic textAlign="left">
        The iGEM TIES project (Team IntEractions Study) is a research study conducted at the Center for Research and Interdisciplinarity (CRI) in Paris. In this project, we study how team interactions and team diversity impact the performance of iGEM teams and the learning experience of the students. How do students collaborate? How are subgroups formed? What is the frequency of interactions with mentors/PIs? How do these interactions lead to better learning (skill spreading), productivity (BioBricks produced / project size), creativity (project uniqueness) or just success in the competition (medals, prizes, winners)?

        To answer these questions, we are partnering with iGEM teams to conduct a quantitative, large-scale study, using several methods:

          <Grid columns={3} stackable textAlign='center'>
            <Grid.Row verticalAlign='middle'>
              <Grid.Column>
                <Header icon>
                  <Icon name="question circle outline" size="massive"/>
                  Questionnaires
                </Header>
                <br/>
                We prepared questionnaires to be filled by team members at the 3 time points during the project. They allow us to better understand the team process and diversity!
              </Grid.Column>

              <Grid.Column>
                <Header icon>
                  <Icon name="expand arrows alternate" size="massive"/>
                  Communication metadata
                </Header>
                <br/>
                The analysis of the metadata (user-timestamp) from communication networks (Slack, WhatsApp, E-mail, etc.) at the end of the project allows to better understand the communication flows within the team.
              </Grid.Column>

              <Grid.Column>
                <Header icon>
                  <Icon name="users" size="massive"/>
                  Proximity experiment
                </Header>
                <br/>
                The analysis of the metadata (user-timestamp) from communication networks (Slack, WhatsApp, E-mail, etc.) at the end of the project allows to better understand the communication flows within the team.
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Segment>
        <Segment vertical textAlign="center">
          All this data is anonymised, kept secured on a server, and participation to each data collection is completely voluntary. After the wiki freeze we provide <b>a report summarizing some general analytics of the team</b> (networks of interaction, number of interactions etc).
          You can find more information about the study in the <a href={information_form}>information form</a> and in some <a href={igemstudy_slides}>background slides</a>. If you have any questions, contact us by email at <a href="mailto:igem-ties@cri-paris.org">igem-ties@cri-paris.org</a>! Also, if you know of other teams that would be interested, spread the word!
        </Segment>
        <Segment basic textAlign="center">
          <Header  as='h1'>Team</Header>
        </Segment>
        <Segment vertical textAlign="left">
          <Grid columns={2} stackable textAlign='center'>
            <Grid.Row verticalAlign='middle'>
              <Grid.Column>
                <Image src={marc_image} avatar centered size="medium"/>
                <Header as="h1">
                  Marc Santolini
                </Header>
                <Header as="h2">Team Leader</Header>
                Marc Santolini is the team leader of the iGEM TIES project. Passionate about science of science, he is a long-term research fellow at CRI Paris, primarily working on network science. He is also the cofounder of Just One Giant Lab (JOGL) and part of the iGEM Insights Steering committee.
              </Grid.Column>
              <Grid.Column>
                <Image src={savvy_image} avatar centered size="medium"/>
                <Header as="h1">
                  Savandara Besse
                </Header>
                <Header as="h2">Communication Manager</Header>
                Savandara Besse aka. Savvy is behind the social media of iGEM-Ties. iGEMer since 2015, she fell in love with the iGEM TIES project and is actively promoting CoSo App to the 2020 iGEM teams. Besides, she is a bioinformatics PhD student at the Université of Montréal. <i color='green'>DM me on iGEM Slack to talk about CoSo app!</i>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row verticalAlign='middle'>
              <Grid.Column>
                <Image src={lionel_image} avatar centered size="medium"/>
                <Header as="h1">
                  Lionel Deveaux
                </Header>
                <Header as="h2">IT Project Manager</Header>
                Lionel Deveaux is a digital project manager at CRI, Paris. His main role is to facilitate IT projects for research, and does so here by coordinating the development and maintenance of CoSo app.
              </Grid.Column>
              <Grid.Column>
                <Image src={rathin_image} avatar centered size="medium"/>
                <Header as="h1">
                  Rathin Jeyaram
                </Header>
                <Header as="h2">Research Engineer</Header>
                Rathin Jeyaram is a research engineer at CRI, Paris. He started as an intern with the iGEM TIES team, and his role is to analyse data from CoSo app to study team science.
              </Grid.Column>
            </Grid.Row>
            <Grid.Row verticalAlign='middle'>
              <Grid.Column>
                <Image src={romain_image} avatar centered size="medium"/>
                <Header as="h1">
                  Romain Delory
                </Header>
                <Header as="h2">UX Designer</Header>
                Romain Delory is designing the user-experience workflow of CoSo app. He is also providing advice on the communication strategy.
              </Grid.Column>
              <Grid.Column>
                <Image src={robbie_image} avatar centered size="medium"/>
                <Header as="h1">
                  Robert Ward
                </Header>
                <Header as="h2">PhD student</Header>
                Robert Ward met Marc and Raphaël during the iGEM Giant Jamboree of 2019. Their common interests of understanding universal properties of scientific and engineering teams leads him to join iGEM TIES. He is working on the task board organization and is building different surveys for the study.
              </Grid.Column>
            </Grid.Row>
            <Grid.Row verticalAlign='middle'>
              <Grid.Column>
                <Image src={leo_image} avatar centered size="medium"/>
                <Header as="h1">
                  Léo Blondel
                </Header>
                <Header as="h2">Tech Wizard</Header>
                Léo Blondel is providing his technical expertises on diverse aspects of CoSo app. He is finishing his PhD in Computational Biology at Harvard university and is also the co-founder with Marc of the open science platform Just One Giant Lab (JOGL).
              </Grid.Column>
              <Grid.Column>
                <Image src={raphael_image} avatar centered size="medium"/>
                <Header as="h1">
                  Raphaël Tackx
                </Header>
                <Header as="h2">Post Doc</Header>
                Raphaël Tackx is a computer scientist interested in social network analysis, in particular how to measure and model human collaborations. He is contributing to the development CoSo app, working mainly on the back-end conception and maintenance.
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Segment>
      </Container>
    );
  }
}

Home.contextType = UserContext;

export default Home;
