import '../App.css';

import React, { useContext, useEffect, useState } from 'react';
import { Container,
         Header,
         Form,
         Segment,
         Icon,
         Button,
         Message } from 'semantic-ui-react';
import { UserContext } from '../utils/UserContext';

import DeleteAccountModal from "../utils/DeleteAccountModal";

import Api from '../Api';

function InlineFormInput({label, icon, loading, value, setValue, onSubmit, infoMessage, setInfoMessage, successMessage}) {

  function handleDismiss() {
    setInfoMessage({success: false, error: ""})
  }

  function onChange(event) {
    setValue(event.target.value)
  }

  return (
    <Form loading={loading}>
      <Form.Input width={16} label={label} iconPosition='left' defaultValue={value} onChange={onChange}>
        <Icon name={icon} />
        <input />
        <Button primary onClick={onSubmit} style={{marginLeft: "10px"}}>Save</Button>
      </Form.Input>
      {infoMessage.success ?
        <Message
          success
          visible
          onDismiss={() => (handleDismiss())}
          icon="checkmark box"
          header={successMessage.header}
          content={successMessage.message}
        />
      : null}
      {infoMessage.error ?
        <Message
          negative
          onDismiss={() => (handleDismiss())}
          icon="frown"
          header="Oh oh"
          content={infoMessage.error}
        />
      : null}
    </Form>
  )
}

function Settings() {

  const userContext = useContext(UserContext);

  useEffect(() => {
    if (userContext.user.email) {
      setEmail(userContext.user.email)
      setFullName(userContext.user.igem_user.full_name)
      setUsername(userContext.user.igem_user.username)
    }
  }, [userContext])



  const [email, setEmail] = useState("")
  const [fullName, setFullName] = useState("")
  const [username, setUsername] = useState("")
  const [password, setPassword] = useState("")
  const [passwordConfirmation, setPasswordConfirmation] = useState("")

  const [userInfoMessage, setUserInfoMessage] = useState({success: false, error: ""})
  const [userInfoLoading, setUserInfoLoading] = useState(false)

  const [igemUserInfoMessage, setIgemUserInfoMessage] = useState({success: false, error: ""})
  const [igemUserInfoLoading, setIgemUserInfoLoading] = useState(false)

  const [emailInfoMessage, setEmailInfoMessage] = useState({success: false, error: ""})
  const [emailInfoLoading, setEmailInfoLoading] = useState(false)

  const [passwordChangeMessage, setPasswordChangeMessage] = useState({success: false, error: ""})
  const [passwordChangeLoading, setPasswordChangeLoading] = useState(false)

  function onEmailSubmit() {
    setEmailInfoLoading(true);
    if (email !== userContext.user.email) {
      const user = {
        email: email
      }
      Api.put("users/" + userContext.user.id, user)
      .then(res => {
        setEmailInfoMessage({success: true, error: ""})
        setEmailInfoLoading(false);
      })
      .catch(error => {
        setEmailInfoMessage({success: false, error: error.messages[0]})
        setEmailInfoLoading(false);
      })
    }
    else {
      setEmailInfoLoading(false);
    }
  }

  function onUsernameSubmit() {
    setIgemUserInfoLoading(true);
    if (username !== userContext.user.igem_user.username) {
      const igem_user = {
        username: username
      }
      Api.put("igem_users/" + userContext.user.igem_user.id, igem_user)
      .then(res => {
        console.log(res)
        userContext.user.igem_user.username = res.data.username
        userContext.setUser(userContext.user)
        setIgemUserInfoMessage({success: true, error: ""})
        setIgemUserInfoLoading(false);
      })
      .catch(error => {
        setIgemUserInfoMessage({success: false, error: error.messages[0]})
        setIgemUserInfoLoading(false);
      })
    }
    else {
      setIgemUserInfoLoading(false);
    }
  }

  function onUserInfoSubmit() {
    setUserInfoLoading(true);
    if (fullName !== userContext.user.igem_user.full_name) {
      const igem_user = {
        full_name: fullName
      }
      Api.put("igem_users/" + userContext.user.igem_user.id, igem_user)
      .then(res => {
        console.log(res)
        userContext.user.igem_user.full_name = res.data.full_name
        userContext.setUser(userContext.user)
        setUserInfoMessage({success: true, error: ""})
        setUserInfoLoading(false);
      })
      .catch(error => {
        setUserInfoMessage({success: false, error: error.messages[0]})
        setUserInfoLoading(false);
      })
    }
    else {
      setUserInfoLoading(false);
    }
  }

  function onPasswordChangeSubmit() {
    setPasswordChangeLoading(true);
    if (password === passwordConfirmation) {
      const user = {
        password: password,
        password_confirmation: passwordConfirmation
      }
      Api.put("users/"+userContext.user.id, {user})
      .then(res => {
        console.log(res)
        userContext.user.igem_user.full_name = res.data.full_name
        userContext.setUser(userContext.user)
        setPasswordChangeMessage({success: true, error: ""})
        setPasswordChangeLoading(false);
      })
      .catch(error => {
        setPasswordChangeMessage({success: false, error: error.messages[0]})
        setPasswordChangeLoading(false);
      })
    }
    else {
      setPasswordChangeLoading(false);
      setPasswordChangeMessage({success: false, error: "Passwords are not the same"})
    }
  }

  function handleDismiss(setter) {
    setter({success: false, error: ""})
  }

  return (
    <Container className="settings-container">
      <Header as="h1">Your user settings and informations</Header>
      <Segment vertical style={{ maxWidth: 450 }}>
        <Header as="h2">Personal information</Header>
        <InlineFormInput
          label="Email"
          icon="at"
          loading={emailInfoLoading}
          value={email}
          setValue={setEmail}
          onSubmit={onEmailSubmit}
          infoMessage={emailInfoMessage}
          setInfoMessage={setEmailInfoMessage}
          successMessage={
            {header: 'New Email saved!',
             message: 'To see the change, please confirm your new email address by clicking on the link your received at your new email.'
           }}
        />
        <InlineFormInput
          label="Your name"
          icon="user"
          loading={userInfoLoading}
          value={fullName}
          setValue={setFullName}
          onSubmit={onUserInfoSubmit}
          infoMessage={userInfoMessage}
          setInfoMessage={setUserInfoMessage}
          successMessage={
            {header: 'User information saved',
             message: 'Go to the dataviz to see the changes reflected.'
           }}
        />
        <InlineFormInput
          label="Your iGEM username"
          icon="user"
          loading={igemUserInfoLoading}
          value={username}
          setValue={setUsername}
          onSubmit={onUsernameSubmit}
          infoMessage={igemUserInfoMessage}
          setInfoMessage={setIgemUserInfoMessage}
          successMessage={
            {header: 'User information saved',
             message: 'Go to the dataviz to see the changes reflected.'
           }}
        />
      </Segment>
      <Segment vertical style={{ maxWidth: 450 }}>
        <Header as="h2">Change your password</Header>
        <Form loading={passwordChangeLoading}>
          <Form.Input type='password' width={16} label="Password" iconPosition='left' defaultValue={password} onChange={(event) => (setPassword(event.target.value))}>
            <Icon name='lock' />
            <input />
          </Form.Input>
          <Form.Input type='password' width={16} label="Confirm password" iconPosition='left' defaultValue={passwordConfirmation} onChange={(event) => (setPasswordConfirmation(event.target.value))}>
            <Icon name='lock' />
            <input />
          </Form.Input>
          {passwordChangeMessage.success ?
            <Message
              success
              visible
              onDismiss={() => (handleDismiss(setPasswordChangeMessage))}
              icon="checkmark box"
              header='Password changed!'
              content='Make sure to remember it ;).'
            />
          : null}
          {passwordChangeMessage.error ?
            <Message
              negative
              onDismiss={() => (handleDismiss(setPasswordChangeMessage))}
              icon="frown"
              header="Oh oh"
              content={passwordChangeMessage.error}
            />
          : null}
          <Button primary onClick={onPasswordChangeSubmit} style={{marginLeft: "10px"}}>Change password</Button>
        </Form>
      </Segment>
      <Segment vertical style={{ maxWidth: 450 }}>
        <Header as="h2">Delete my account</Header>
        If you want to delete your account and all data associated with it click the red button below.
        <br />
        <DeleteAccountModal />
      </Segment>
    </Container>
  );
}

export default Settings;
