import './App.css';
import React, { useState, useEffect } from 'react';
import Insights from "./pages/Insights"
import Home from "./pages/Home"
import Team from "./pages/Team"
import SurveysPage from "./pages/Surveys"
import NewsPage from "./pages/News"
import WebViews from "./pages/Webviews"
import Settings from "./pages/Settings"
import Admin from "./pages/Admin"
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import ResponsiveContainer from "./utils/ResponsiveContainer"
import { UserContext } from './utils/UserContext';
import { Header } from "./components/Header";
import { Footer } from "./components/Footer";
import Api from "./Api"
function App() {
  const [user, setUser] = useState({})
  const [firstLoad, setFirstLoad] = useState(true)

  useEffect(() => {
    const user_data = JSON.parse(sessionStorage.getItem("User") || localStorage.getItem("User"));
    if (firstLoad) {
      if (user_data) {
        setUser(user_data)
        Api.get("users/" + user_data.id)
        .then(res => {
          setUser(res.data)
        })
        .catch(error => {
          console.log("Error on getting initial user data")
          if (error.status === 401) {
            setUser({})
          }
        })
      }
      else {
        setUser({})
      }
      setFirstLoad(false)
    }
  }, [firstLoad]);

  return (
    <UserContext.Provider value={{user: user, setUser: setUser}}>
      <Router>
        <Switch>
          <Route path="/views">
            <WebViews />
          </Route>
        <ResponsiveContainer>
          <section className="main">
            <Route path="/settings">
              <Settings />
            </Route>
            <Route path="/team">
              <Team />
            </Route>
            <Route path="/insights">
              <Insights />
            </Route>
            <Route path="/news">
              <NewsPage />
            </Route>
            <Route path="/surveys">
              <SurveysPage />
            </Route>
            <Route path="/admin">
              <Admin />
            </Route>
            <Route exact path="/">
              <Header/>
              <Home />
            </Route>
          </section>
          <Footer />
        </ResponsiveContainer>
        </Switch>
      </Router>
     </UserContext.Provider>
  );
}

export default App;
